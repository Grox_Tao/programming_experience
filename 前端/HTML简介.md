# HTML简介

## 1. 网页

### 1. 什么是网页

**网站**是指在因特网上根据一定的规则,使用HTML等制作的用于展示特定内容相关的网页集合.

**网页**是网站中的一页,通常是HTML格式的文件,它也要通过浏览器来阅读.

![image-20200731160653779](C:\Users\Grox\AppData\Roaming\Typora\typora-user-images\image-20200731160653779.png)