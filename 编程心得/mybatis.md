Mybatis

需要掌握:

+ JDBC
+ Mysql
+ Java基础
+ Maven
+ Junit

[mybatis中文社区](https://mybatis.org/mybatis-3/)

## 1. 简介

### 1.1. 什么是mybatis

![image-20200715085922047](G:\JYGS\dir\programming_experience\编程心得\mybatis.assets\image-20200715085922047.png)

+ MyBatis 是一款优秀的**持久层框架**;
+ 它支持自定义 SQL、存储过程以及高级映射。
+ MyBatis 免除了几乎所有的 JDBC 代码以及设置参数和获取结果集的工作。
+ MyBatis 可以通过简单的 XML 或注解来配置和映射原始类型、接口和 Java POJO（Plain Old Java Objects，普通老式 Java 对象）为数据库中的记录。
+ MyBatis 本是[apache](https://baike.baidu.com/item/apache/6265)的一个开源项目[iBatis](https://baike.baidu.com/item/iBatis), 2010年这个项目由apache software foundation 迁移到了google code，并且改名为MyBatis 。
+ 2013年11月迁移到Github。



如何获取Mybatis?

+ Maven仓库

```xml
<!--mybatis-->
        <dependency>
            <groupId>org.mybatis</groupId>
            <artifactId>mybatis</artifactId>
            <version>3.5.2</version>
        </dependency>
```

+ Github : https://github.com/mybatis.mybatis-3/releases
+ 中文文档 : https://mybatis.org/mybatis-3/zh/index.html

### 1.2. 持久化(动作)

数据持久化

+ 持久化就是将程序的数据在持久状态和瞬时状态转换的过程
+ 内存特点 : **断电即失**
+ 数据库(jdbc), io文件持久化(==浪费资源==)

**为什么需要持久化**

有一些对象,不想让他丢掉

### 1.3. 持久层(概念)

我们以前认识了DAO层,Service层,Controller层

+ 完成持久化工作的代码块
+ 层界限十分明显

### 1.4. 为什么需要Mybatis?( 技术没有高低之分 )

+ 帮助程序员将数据存入数据库中
+ 方便
+ 传统的JDBC代码太复杂了
+ 容易上手
+ 优点
  + 简单易学
  + 灵活
  + sql和代码分离,降低耦合,提高了可维护性
  + 提供映射标签, 支持对象与数据库的[orm](https://blog.csdn.net/zhangchibang311/article/details/83329802?utm_medium=distribute.pc_relevant.none-task-blog-BlogCommendFromMachineLearnPai2-7.nonecase&depth_1-utm_source=distribute.pc_relevant.none-task-blog-BlogCommendFromMachineLearnPai2-7.nonecase)字段关系映射[^1]
  + 提供对象关系映射标签, 支持对象关系组建维护
  + 提供xml标签, 支持编写动态sql



## 2. 第一个Mybatis程序

思路: 搭建环境-->导入Mybatis-->编写代码-->测试!

### 2.1. 搭建环境

1. 搭建数据库

```sql
create database 'mybatis';
use mybatis;
create table 'user'(
	`id` int(20) not null primary key,
    `name` varchar(30) default null,
    `pwd` varchar(30) default null 
)ENGINE=innodb default charset=utf8;

-- 插入数据
insert into `user` (`id`,`name`,`pwd`) value
(1,'迪丽热巴','12345'),
(2,'古力娜扎','23456'),
(3,'马尔扎哈','34567')
```

![image-20200715113341431](\编程心得\mybatis.assets\image-20200715113341431.png)



2. 创建maven工程
   1. 新建一个普通的maven项目
   2. 删除src目录
   3. 导入maven依赖

```xml
<!--导入依赖-->
    <dependencies>
        <!--mysql驱动-->
        <dependency>
            <groupId>mysql</groupId>
            <artifactId>mysql-connector-java</artifactId>
            <version>5.1.47</version>
        </dependency>
        <!--mybatis-->
        <dependency>
            <groupId>org.mybatis</groupId>
            <artifactId>mybatis</artifactId>
            <version>3.5.2</version>
        </dependency>
        <!--junit-->
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>4.12</version>
        </dependency>
    </dependencies>
```

### 2.2. 创建一个模块

#### 1. 新建一个maven模块

#### 2. 在resources中创建mybatis-config.xml文件(mybatis核心配置文件)

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE configuration
  PUBLIC "-//mybatis.org//DTD Config 3.0//EN"
  "http://mybatis.org/dtd/mybatis-3-config.dtd">
<!--configuration核心配置文件-->
<!--environments 开发环境,可以有多套环境-->
<!--transactionManager 事务管理 默认jdbc--> 
<!--configuration核心配置文件-->
<configuration>
  <!--environments 开发环境,可以有多套环境-->
  <environments default="development">
    <environment id="development">
      <!--transactionManager 事务管理 默认jdbc-->
      <transactionManager type="JDBC"/>
      <dataSource type="POOLED">
        <property name="driver" value="com.mysql.jdbc.Driver"/>
        <property name="url" value="jdbc:mysql://localhost:3306/test?useSSL=true&amp;useUNnicode=true&amp;characterEncoding=UTF-8"/>
        <property name="username" value="root"/>
        <property name="password" value="root"/>
      </dataSource>
    </environment>
  </environments>
<!-- 每一个Mapper.XML都需要在Mybatis的核心配置文件上注册 -->
  <mappers>
    <mapper resource="com/grox/mapper/UserDao.xml"/>
  </mappers>
</configuration>
```

#### 3. 编写mybatis工具类

```java
public class MybatisUtils {

    private static SqlSessionFactory sqlSessionFactory;

    static {
        InputStream inputStream = null;
        try {
            // 使用Mybatis的第一步,获取SqlSessionFactory对象
            String resource = "mybatis-config.xml";
            inputStream = Resources.getResourceAsStream(resource);
            sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // 既然有了 SqlSessionFactory，顾名思义，我们可以从中获得 SqlSession 的实例。
    // SqlSession 提供了在数据库执行 SQL 命令所需的所有方法。你可以通过 SqlSession 实例来直接执行已映射的 SQL 语句
    public static SqlSession getSqlSession(){
        return sqlSessionFactory.openSession();
    }
}
```

### 2.3. 编写代码

+ 实体类

```java
public class User {
    private Integer id;
    private String name;
    private Integer sex;
    private String password;

    public User() {
    }

    public User(Integer id, String name, Integer sex, String password) {
        this.id = id;
        this.name = name;
        this.sex = sex;
        this.password = password;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", sex=" + sex +
                ", password='" + password + '\'' +
                '}';
    }
}
```

+ Dao接口

```java
public interface UserDao {

    List<User> getUserList();
}
```

+ 接口实现类

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper
  PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN"
  "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="com.grox.mapper.UserDao">
  <select id="getUserList" resultType="com.grox.model.User">
    select * from user
  </select>
</mapper>
```

### 2.4. 测试(junit)

```java
public class UserDaoTest {

    @Test
    public void test(){
        SqlSession sqlSession = MybatisUtils.getSqlSession();
        UserDao mapper = sqlSession.getMapper(UserDao.class);
        List<User> userList = mapper.getUserList();
        for (User user : userList) {
            System.out.println("user = " + user);
        }

        sqlSession.close();
    }
}
```

**会出现的问题**

**1. 配置文件没有注册**

**2. 绑定接口错误 **

**3. 方法名不对**

**4. 返回类型不对**

## 3. CRUD

### 1. namespace

namespace中的包名 要和Dao/Mapper接口的包名一致!

### 2. select

选择,查询语句:

+ id : 就是对应的namespace中的方法名
+ resultType : Sql语句执行的返回值!
+ parameterType : 参数类型

> 1. 编写接口

```java
// 获取全部人员
    List<User> getUserList();
    // 根据id获取人员
    User getUserById(Integer id);
```



> 2. 编写对应的mapper中的sql语句

```xml
<select id="getUserList" resultType="com.grox.model.User">
        select *
        from user
    </select>

    <select id="getUserById" resultType="com.grox.model.User" parameterType="integer">
        select *
        from user
        where id = #{id,jdbcType=INTEGER}
    </select>
```



> 3. 测试

```java
 @Test
    public void selectAll() {
        SqlSession sqlSession = MybatisUtils.getSqlSession();
        UserDao mapper = sqlSession.getMapper(UserDao.class);
        List<User> userList = mapper.getUserList();
        for (User user : userList) {
            System.out.println("user = " + user);
        }

        sqlSession.close();
    }

    @Test
    public void getUserById() {
        SqlSession sqlSession = MybatisUtils.getSqlSession();
        UserDao mapper = sqlSession.getMapper(UserDao.class);
        User user = mapper.getUserById(5);

        System.out.println("user = " + user);


        sqlSession.close();
    }
```



### 3. insert

> 1. 编写接口

```java
// 新增
    int add(User user);
```



> 2. 编写对应的mapper中的sql语句

```xml
<insert id="add" parameterType="com.grox.model.User">
        insert into user
        <trim prefix="(" suffix=")" suffixOverrides=",">
            <if test="id != null and id != 0">
                id,
            </if>
            <if test="name != null and name != ''">
                name,
            </if>

            <if test="sex != null and sex != 0">
                sex,
            </if>
            <if test="password != null and password != ''">
                password,
            </if>
        </trim>
        <trim prefix="value(" suffix=")" suffixOverrides=",">
            <if test="id != null and id != 0">
                #{id,jdbcType=INTEGER},
            </if>
            <if test="name != null and name != ''">
                #{name,jdbcType=VARCHAR},
            </if>

            <if test="sex != null and sex != 0">
                #{sex,jdbcType=INTEGER},
            </if>
            <if test="password != null and password != ''">
                #{password,jdbcType=VARCHAR}
            </if>
        </trim>
    </insert>
```



> 3. 测试

```java
@Test
    public void add() {
        SqlSession sqlSession = MybatisUtils.getSqlSession();
        UserDao mapper = sqlSession.getMapper(UserDao.class);
        User user = new User();
        user.setId(5);
        user.setName("陶云鹏");
        user.setSex(1);
        user.setPassword("12345");
        int add = mapper.add(user);
        if (add == 1) {
            System.out.println("添加成功!");
        } else {
            System.out.println("添加失败!");
        }
        sqlSession.commit();  // 注意,新增需要进行事务提交,否则无法新增.
        sqlSession.close();
    }
```



### 4. update

> 1. 编写接口

```java
 // 修改
    int update(User user);
```



> 2. 编写对应的mapper中的sql语句

```xml
<update id="update" parameterType="com.grox.model.User">
        update user
        <set>
            <if test="name != null and name != ''">
                name = #{name,jdbcType=VARCHAR},
            </if>
            <if test="sex != null and sex != 0">
                sex = #{sex,jdbcType=INTEGER},
            </if>
            <if test="password != null and password != ''">
                password = #{password,jdbcType=VARCHAR}
            </if>
        </set>
        where id = #{id,jdbcType=INTEGER}
    </update>
```



> 3. 测试

```java
@Test
    public void update() {
        SqlSession sqlSession = MybatisUtils.getSqlSession();
        UserDao mapper = sqlSession.getMapper(UserDao.class);
        User user = new User();
        user.setId(5);
        user.setName("陶清浅");
        user.setSex(1);
        user.setPassword("1234567");
        int update = mapper.update(user);
        if (update == 1) {
            System.out.println("修改成功!");
        } else {
            System.out.println("修改失败!");
        }
        sqlSession.commit(); // 注意,修改需要进行事务提交,否则无法修改.
        sqlSession.close();
    }
```



### 5. delete

> 1. 编写接口

```java
    // 删除
    int delete(int id);
```



> 2. 编写对应的mapper中的sql语句

```xml
<delete id="delete">
        delete
        from user
        where id = #{id}
    </delete>
```



> 3. 测试

```java
@Test
    public void delete() {
        SqlSession sqlSession = MybatisUtils.getSqlSession();
        UserDao mapper = sqlSession.getMapper(UserDao.class);
        User user = new User();
        int delete = mapper.delete(1);
        if (delete == 1) {  //如果是批量删除,则可以改为>0
            System.out.println("删除成功!");
        } else {
            System.out.println("删除失败!");
        }
        sqlSession.commit();  // 注意,删除需要进行事务提交,否则无法删除.
        sqlSession.close();
    }
```



### 6. 万能Map



```
因数据库5.7.28升级到8.0.21版本防安全漏洞，测试环境83上部署了8.0.21的数据库，请各模块在datasource的url坐下修改，并重启服务验证连接新数据库后是否功能正常，
```



假设,我们的实体类,或者数据库中的表,字段过多或者参数过多,我们应当考虑使用Map!

```java
@Test
    public void add2() {
        SqlSession sqlSession = MybatisUtils.getSqlSession();
        UserDao mapper = sqlSession.getMapper(UserDao.class);
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("userId",6);
        map.put("userName","郭鑫");
        map.put("xingbie",0);
        map.put("pwd","20191219");
        int add = mapper.add2(map);
        if (add == 1) {
            System.out.println("添加成功!");
        } else {
            System.out.println("添加失败!");
        }
        sqlSession.commit();
        sqlSession.close();
    }
```

```java
// 新增(万能Map版)
    int add2(Map<String,Object> map);
```

```xml
<insert id="add2" parameterType="map">
        insert into user
        <trim prefix="(" suffix=")" suffixOverrides=",">
            <if test="userId != null and userId != 0">
                id,
            </if>
            <if test="userName != null and userName != ''">
                name,
            </if>
            <if test="xingbie != null">
                sex,
            </if>
            <if test="pwd != null and pwd != ''">
                password,
            </if>
        </trim>
        <trim prefix="value(" suffix=")" suffixOverrides=",">
            <if test="userId != null and userId != 0">
                #{userId},
            </if>
            <if test="userName != null and userName != ''">
                #{userName},
            </if>

            <if test="xingbie != null">
                #{xingbie},
            </if>
            <if test="pwd != null and pwd != ''">
                #{pwd}
            </if>
        </trim>
    </insert>
```

>Map传递参数,直接在sql中取出key即可!
>
>对象传递参数,直接在sql中对象的属性即可!
>
>只有一个基本类型参数的情况下,可以直接在sql中取到

### 7. 思考

模糊查询怎么写?

1. Java代码执行的时候,传递通配符%%

```java
List<User> userlist = mapper.getUserLike("%李%");
```

2. 在sql拼接中使用通配符

```xml
select * from user where name = "%"#{name}"%"
```



## 4. 配置解析

![image-20200721105550380](G:\JYGS\dir\programming_experience\编程心得\mybatis.assets\image-20200721105550380.png)

### 1.核心配置文件

+ mybaits-config.xml
+ Mybatis的配置文件包含了会深深影响Mybatis行为的设置和属性信息

```
configuration（配置）
properties（属性）
settings（设置）
typeAliases（类型别名）
typeHandlers（类型处理器）
objectFactory（对象工厂）
plugins（插件）
environments（环境配置）
environment（环境变量）
transactionManager（事务管理器）
dataSource（数据源）
databaseIdProvider（数据库厂商标识）
mappers（映射器）
```

### 2. environments（环境配置）

MyBatis 可以配置成适应多种环境

**不过要记住：尽管可以配置多个环境，但每个 SqlSessionFactory 实例只能选择一种环境。**

###  3. properties（属性）

我们可以通过properties竖向来实现引用配置文件

这些属性都是可外部配置且可动态替换的,既可以在典型的Java属性文件中配置,亦可通过properties元素的子元素来传递

[db.properties]

1. 编写一个配置文件

```java
driver=com.mysql.jdbc.Driver
url=jdbc:mysql://localhost:3306/test?useSSL=true&useUnicode=true&characterEncoding=UTF-8
username=root
password=root
```

2. 在核心配置文件中引入

```xml
 <!--引入外部配置文件-->
  <properties resource="db.properties"/>
```

+ 可以直接引入外部文件
+ 可以在其中增加一些属性配置
+ 如果两个文件有同一个字段,优先使用我不配置文件的

### 4. typeAliases（类型别名）

类型别名可为 Java 类型设置一个缩写名字。 它仅用于 XML 配置，意在降低冗余的全限定类名书写。

```xml
<typeAliases>
    <typeAlias type="com.grox.model.User" alias="User"/>
  </typeAliases>
```

也可指令一个包名,Mybatis会在包名下搜索所有的Java Bean,比如扫描实体类的包,他的默认别名就是这个类的类名,首字母小写

```xml
<typeAliases>
    <tpackage name="com.grox.model"/>
  </typeAliases>
```

在实体类比较少的时候,使用第一种方式.

如果实体类较多,建议使用第二种方式.

### 5. settings（设置）

这是 MyBatis 中极为重要的调整设置，它们会改变 MyBatis 的运行时行为



|              设置名              |                             描述                             |                            有效值                            |                        默认值                         |
| :------------------------------: | :----------------------------------------------------------: | :----------------------------------------------------------: | :---------------------------------------------------: |
|           cacheEnabled           |   全局性地开启或关闭所有映射器配置文件中已配置的任何缓存。   |                        true \| false                         |                         true                          |
|        lazyLoadingEnabled        | 延迟加载的全局开关。当开启时，所有关联对象都会延迟加载。 特定关联关系中可通过设置 `fetchType` 属性来覆盖该项的开关状态。 |                        true \| false                         |                         false                         |
|      aggressiveLazyLoading       | 开启时，任一方法的调用都会加载该对象的所有延迟加载属性。 否则，每个延迟加载属性会按需加载（参考 `lazyLoadTriggerMethods`)。 |                        true \| false                         |     false （在 3.4.1 及之前的版本中默认为 true）      |
|    multipleResultSetsEnabled     |     是否允许单个语句返回多结果集（需要数据库驱动支持）。     |                        true \| false                         |                         true                          |
|          useColumnLabel          | 使用列标签代替列名。实际表现依赖于数据库驱动，具体可参考数据库驱动的相关文档，或通过对比测试来观察。 |                        true \| false                         |                         true                          |
|         useGeneratedKeys         | 允许 JDBC 支持自动生成主键，需要数据库驱动支持。如果设置为 true，将强制使用自动生成主键。尽管一些数据库驱动不支持此特性，但仍可正常工作（如 Derby）。 |                        true \| false                         |                         False                         |
|       autoMappingBehavior        | 指定 MyBatis 应如何自动映射列到字段或属性。 NONE 表示关闭自动映射；PARTIAL 只会自动映射没有定义嵌套结果映射的字段。 FULL 会自动映射任何复杂的结果集（无论是否嵌套）。 |                     NONE, PARTIAL, FULL                      |                        PARTIAL                        |
| autoMappingUnknownColumnBehavior | 指定发现自动映射目标未知列（或未知属性类型）的行为。`NONE`: 不做任何反应`WARNING`: 输出警告日志（`'org.apache.ibatis.session.AutoMappingUnknownColumnBehavior'` 的日志等级必须设置为 `WARN`）`FAILING`: 映射失败 (抛出 `SqlSessionException`) |                    NONE, WARNING, FAILING                    |                         NONE                          |
|       defaultExecutorType        | 配置默认的执行器。SIMPLE 就是普通的执行器；REUSE 执行器会重用预处理语句（PreparedStatement）； BATCH 执行器不仅重用语句还会执行批量更新。 |                      SIMPLE REUSE BATCH                      |                        SIMPLE                         |
|     defaultStatementTimeout      |     设置超时时间，它决定数据库驱动等待数据库响应的秒数。     |                          任意正整数                          |                     未设置 (null)                     |
|         defaultFetchSize         | 为驱动的结果集获取数量（fetchSize）设置一个建议值。此参数只可以在查询设置中被覆盖。 |                          任意正整数                          |                     未设置 (null)                     |
|       defaultResultSetType       |           指定语句默认的滚动策略。（新增于 3.5.2）           | FORWARD_ONLY \| SCROLL_SENSITIVE \| SCROLL_INSENSITIVE \| DEFAULT（等同于未设置） |                     未设置 (null)                     |
|       safeRowBoundsEnabled       | 是否允许在嵌套语句中使用分页（RowBounds）。如果允许使用则设置为 false。 |                        true \| false                         |                         False                         |
|     safeResultHandlerEnabled     | 是否允许在嵌套语句中使用结果处理器（ResultHandler）。如果允许使用则设置为 false。 |                        true \| false                         |                         True                          |
|     mapUnderscoreToCamelCase     | 是否开启驼峰命名自动映射，即从经典数据库列名 A_COLUMN 映射到经典 Java 属性名 aColumn。 |                        true \| false                         |                         False                         |
|         localCacheScope          | MyBatis 利用本地缓存机制（Local Cache）防止循环引用和加速重复的嵌套查询。 默认值为 SESSION，会缓存一个会话中执行的所有查询。 若设置值为 STATEMENT，本地缓存将仅用于执行语句，对相同 SqlSession 的不同查询将不会进行缓存。 |                     SESSION \| STATEMENT                     |                        SESSION                        |
|         jdbcTypeForNull          | 当没有为参数指定特定的 JDBC 类型时，空值的默认 JDBC 类型。 某些数据库驱动需要指定列的 JDBC 类型，多数情况直接用一般类型即可，比如 NULL、VARCHAR 或 OTHER。 |       JdbcType 常量，常用值：NULL、VARCHAR 或 OTHER。        |                         OTHER                         |
|      lazyLoadTriggerMethods      |             指定对象的哪些方法触发一次延迟加载。             |                    用逗号分隔的方法列表。                    |            equals,clone,hashCode,toString             |
|     defaultScriptingLanguage     |            指定动态 SQL 生成使用的默认脚本语言。             |                  一个类型别名或全限定类名。                  | org.apache.ibatis.scripting.xmltags.XMLLanguageDriver |
|      defaultEnumTypeHandler      |    指定 Enum 使用的默认 `TypeHandler` 。（新增于 3.4.5）     |                  一个类型别名或全限定类名。                  |        org.apache.ibatis.type.EnumTypeHandler         |
|        callSettersOnNulls        | 指定当结果集中值为 null 的时候是否调用映射对象的 setter（map 对象时为 put）方法，这在依赖于 Map.keySet() 或 null 值进行初始化时比较有用。注意基本类型（int、boolean 等）是不能设置成 null 的。 |                        true \| false                         |                         false                         |
|    returnInstanceForEmptyRow     | 当返回行的所有列都是空时，MyBatis默认返回 `null`。 当开启这个设置时，MyBatis会返回一个空实例。 请注意，它也适用于嵌套的结果集（如集合或关联）。（新增于 3.4.2） |                        true \| false                         |                         false                         |
|            logPrefix             |             指定 MyBatis 增加到日志名称的前缀。              |                          任何字符串                          |                        未设置                         |
|             logImpl              |    指定 MyBatis 所用日志的具体实现，未指定时将自动查找。     | SLF4J \| LOG4J \| LOG4J2 \| JDK_LOGGING \| COMMONS_LOGGING \| STDOUT_LOGGING \| NO_LOGGING |                        未设置                         |
|           proxyFactory           |      指定 Mybatis 创建可延迟加载对象所用到的代理工具。       |                      CGLIB \| JAVASSIST                      |            JAVASSIST （MyBatis 3.3 以上）             |
|             vfsImpl              |                       指定 VFS 的实现                        |         自定义 VFS 的实现的类全限定名，以逗号分隔。          |                        未设置                         |
|        useActualParamName        | 允许使用方法签名中的名称作为语句参数名称。 为了使用该特性，你的项目必须采用 Java 8 编译，并且加上 `-parameters` 选项。（新增于 3.4.1） |                        true \| false                         |                         true                          |
|       configurationFactory       | 指定一个提供 `Configuration` 实例的类。 这个被返回的 Configuration 实例用来加载被反序列化对象的延迟加载属性值。 这个类必须包含一个签名为`static Configuration getConfiguration()` 的方法。（新增于 3.2.3） |                 一个类型别名或完全限定类名。                 |                        未设置                         |
|      shrinkWhitespacesInSql      | Removes extra whitespace characters from the SQL. Note that this also affects literal strings in SQL. (Since 3.5.5) |                        true \| false                         |                         false                         |

### 6. 其他配置

+ typeHandlers（类型处理器）
+ objectFactory（对象工厂）
+ plugins（插件）
  + mybatis-generator-core  (可以根据数据库,自动生成sql脚本)
  + mybatis-plus (mybaits的互补插件,提升mybatis的性能)
  + 通用mapper

### 7. mappers（映射器）

既然 MyBatis 的行为已经由上述元素配置完了，我们现在就要来定义 SQL 映射语句了。 但首先，我们需要告诉 MyBatis 到哪里去找到这些语句。 在自动查找资源方面，Java 并没有提供一个很好的解决方案，所以最好的办法是直接告诉 MyBatis 到哪里去找映射文件。 你可以使用相对于类路径的资源引用，或完全限定资源定位符（包括 `file:///` 形式的 URL），或类名和包名等。例如：

```xml
<!-- 使用相对于类路径的资源引用 -->
<mappers>
  <mapper resource="org/mybatis/builder/AuthorMapper.xml"/>
  <mapper resource="org/mybatis/builder/BlogMapper.xml"/>
  <mapper resource="org/mybatis/builder/PostMapper.xml"/>
</mappers>
<!-- 使用完全限定资源定位符（URL）[不建议使用] -->  
<mappers>
  <mapper url="file:///var/mappers/AuthorMapper.xml"/>
  <mapper url="file:///var/mappers/BlogMapper.xml"/>
  <mapper url="file:///var/mappers/PostMapper.xml"/>
</mappers>
<!-- 使用映射器接口实现类的完全限定类名 -->
<!-- 注意: 1.接口和他的Mapper配置文件必须同名 2.接口和他的mapper配置文件必须在同一个包下  -->
<mappers>
  <mapper class="org.mybatis.builder.AuthorMapper"/>
  <mapper class="org.mybatis.builder.BlogMapper"/>
  <mapper class="org.mybatis.builder.PostMapper"/>
</mappers>
<!-- 将包内的映射器接口实现全部注册为映射器 -->
<!-- 注意: 1.接口和他的Mapper配置文件必须同名 2.接口和他的mapper配置文件必须在同一个包下  -->
<mappers>
  <package name="org.mybatis.builder"/>
</mappers>
```

### 8.  生命周期和作用域

![image-20200721171557738](G:\JYGS\dir\programming_experience\编程心得\mybatis.assets\image-20200721171557738.png)

#### SqlSessionFactoryBuilder

+ 这个类可以被实例化、使用和丢弃，一旦创建了 SqlSessionFactory，就不再需要它了。
+  作用域是方法作用域（也就是局部方法变量）。 
+ 你可以重用 SqlSessionFactoryBuilder 来创建多个 SqlSessionFactory 实例，但最好还是不要一直保留着它，以保证所有的 XML 解析资源可以被释放给更重要的事情。

#### SqlSessionFactory

+ 说白了就是数据库连接池

+ SqlSessionFactory 一旦被创建就应该在应用的运行期间一直存在，没有任何理由丢弃它或重新创建另一个实例。
+  使用 SqlSessionFactory 的最佳实践是在应用运行期间不要重复创建多次，多次重建 SqlSessionFactory 被视为一种代码“坏习惯”。
+ 因此 SqlSessionFactory 的最佳作用域是应用作用域
+ 最简单的就是使用单例模式或者静态单例模式。

#### SqlSession

+ 链接池的一个请求
+ SqlSession 的实例不是线程安全的，因此是不能被共享的，所以它的最佳的作用域是请求或方法作用域
+ 用完立刻关闭,否则资源被占用.



![image-20200721171741903](G:\JYGS\dir\programming_experience\编程心得\mybatis.assets\image-20200721171741903.png)

每一个mapper都代表一个具体的业务



## 5. resultMap

## 6. 日志

### 1. 日志工厂

日志种类

>  SLF4J | LOG4J | LOG4J2 | JDK_LOGGING | COMMONS_LOGGING | STDOUT_LOGGING | NO_LOGGING

如果一个数据库操作出现了异常,我们需要排错,日志就是最好的助手!

以前我们会用控制台输出和debug进行排除,今天我们讲讲如何用日志进行排错!

```xml
// 在mybatis核心配置文件中增加settings属性,使用logImpl添加日志
// 注意,在核心配置中,属性的位置有严格的要求
<settings>
      <setting name="logImpl" value="STDOUT_LOGGING"/>
    </settings>
```

> 属性位置

![image-20200722093650121](G:\JYGS\dir\programming_experience\编程心得\mybatis.assets\image-20200722093650121.png)



**STDOUT_LOGGING (标准日志输出)**

![image-20200722094859238](G:\JYGS\dir\programming_experience\编程心得\mybatis.assets\image-20200722094859238.png)



### 2. log4j

什么是log4j?

+ Log4j是Apache的一个开源项目,通过使用Log4j,我们可以控制日志信息输送的目的地是控制台,文件,GUI组件
+ 我们也可以控制每一条一致的输出格式;
+ 通过定义每一条日志信息的级别,我们能够更加细致的控制日志的生成过程
+ 我们还可以通过一个配置来灵活的进行配置,而不需要修改应用的代码.

1. 首先导入jar包

```xml
<!-- https://mvnrepository.com/artifact/log4j/log4j -->
        <dependency>
            <groupId>log4j</groupId>
            <artifactId>log4j</artifactId>
            <version>1.2.17</version>
        </dependency>
```

2. 编写log4j.properties

```properties
# 将等级为DEBUG的日志信息输出到console和file这两个目的地,console和file的定义在下面的代码
log4j.rootLogger=DEBUG,console,file
#控制台输出的相关设置
log4j.appender.console = org.apache.log4j.ConsoleAppender
log4j.appender.console.Target = System.out
log4j.appender.console.Threshold=DEBUG
log4j.appender.console.layout=org.apache.log4j.PatternLayout
log4j.appender.console.layout.ConversionPattern=[%c]-%m%n

#文件输出的相关设置
log4j.appender.file = org.apache.log4j.RollingFileAppender
log4j.appender.file.File=./log/grox.log
log4j.appender.file.MaxFileSize=10mb
log4j.appender.file.Threshould=DEBUG
log4j.appender.file.layout=org.apache.log4j.PatternLayout
log4j.appender.file.layout.ConversionPattern=[%p][%d{yy-MM-dd}][%c]%m%n
#日志输出级别
log4j.logger.org.mybatis=DEBUG
log4j.logger.java.sql=DEBUG
log4j.logger.java.sql.Statement=DEBUG
log4j.logger.java.sql.ResultSet=DEBUG
log4j.logger.java.sql.PreparStatement=DEBUG
```

3. 配置log4j为日志的实现

```xml
<settings>
      <setting name="logImpl" value="LOG4J"/>
    </settings>
```

4. 直接测试

![image-20200723141104625](G:\JYGS\dir\programming_experience\编程心得\mybatis.assets\image-20200723141104625.png)

**请自行翻译日志中红框中的数据**

5. 简单实用log

   1. 在类中导入包

      ```java
      import org.apache.log4j.Logger
      ```

   2. 创建日志对象,参数为当前类的class

      ```java
      // 该对象的定义在全局对象的位置
      static Logger logger = Logger.getLogger(本类的类名.class)
      ```

   3. 日志级别

      ```
      log4j定义了8个级别的log（除去OFF和ALL，可以说分为6个级别），优先级从高到低依次为：OFF、FATAL、ERROR、WARN、INFO、DEBUG、TRACE、 ALL。
      
      ALL 最低等级的，用于打开所有日志记录。
      
      TRACE designates finer-grained informational events than the DEBUG.Since:1.2.12，很低的日志级别，一般不会使用。
      
      DEBUG 指出细粒度信息事件对调试应用程序是非常有帮助的，主要用于开发过程中打印一些运行信息。
      
      INFO 消息在粗粒度级别上突出强调应用程序的运行过程。打印一些你感兴趣的或者重要的信息，这个可以用于生产环境中输出程序运行的一些重要信息，但是不能滥用，避免打印过多的日志。
      
      WARN 表明会出现潜在错误的情形，有些信息不是错误信息，但是也要给程序员的一些提示。
      
      ERROR 指出虽然发生错误事件，但仍然不影响系统的继续运行。打印错误和异常信息，如果不想输出太多的日志，可以使用这个级别。
      
      FATAL 指出每个严重的错误事件将会导致应用程序的退出。这个级别比较高了。重大错误，这种级别你可以直接停止程序了。
      
      OFF 最高等级的，用于关闭所有日志记录。
      
      如果将log level设置在某一个级别上，那么比此级别优先级高的log都能打印出来。例如，如果设置优先级为WARN，那么OFF、FATAL、ERROR、WARN 4个级别的log能正常输出，而INFO、DEBUG、TRACE、 ALL级别的log则会被忽略。Log4j建议只使用四个级别，优先级从高到低分别是ERROR、WARN、INFO、DEBUG。
      
      从我们实验的结果可以看出，log4j默认的优先级为ERROR或者WARN（实际上是ERROR）。
      ```

      

## 7. 分页

**为什么分页**

+ 减少数据的处理量

### 1. limit分页

```xml
<select id="getUserByLimit" resultType="com.grox.model.User" parameterType="map">
      select * from user limit #{startIndex},#{pageSize}
    </select>
```

```java
List<User> getUserByLimit(Map<String, Integer> map);
```

```java
@Test
    public void getUseByLimit() {
        SqlSession sqlSession = MybatisUtils.getSqlSession();
        UserDao mapper = sqlSession.getMapper(UserDao.class);
        Map<String, Integer> map = new HashMap<String, Integer>();
        map.put("startIndex", 0);
        map.put("pageSize", 5);
        List<User> userList = mapper.getUserByLimit(map);
        for (User user : userList) {
            System.out.println("user = " + user);
        }

        sqlSession.close();
    }
```

### 2. RowBounds分页

**sql上不做分页语句,转而将分页放到了代码层面实现**

```xml
<select id="getUserRowbounds" resultType="com.grox.model.User">
      select * from user
  </select>
```

```java
	// rowbounds分页
    List<User> getUserRowbounds();
```

```java
@Test
    public void getUseRowBounds() {
        SqlSession sqlSession = MybatisUtils.getSqlSession();
        // rowbounds 实现分页
        RowBounds rowBounds = new RowBounds(2, 5);
        // 通过代码层面实现分页
        List<User> userlist = sqlSession.selectList("com.grox.mapper.UserDao.getUserRowbounds",null,rowBounds);
        for (User user : userlist) {
            System.out.println("user = " + user);
        }
        sqlSession.close();
    }
```



### 3. 分页插件

![image-20200724112542048](mybatis.assets/image-20200724112542048.png)



## 8. 注解开发

### 1. 面向接口编程

大家以前都学过面向对象编程,也学过接口,但在真正的开发中,很多时候我们会选择面向接口编程

其根本原因是: **解耦,可拓展,提高复用,在分层开发中,上层不用管具体的实现,大家都遵守共同的标准,使得开发变的容易,有规范性**

在一个面向对象的系统中,系统的各种功能是由许许多多的不同对象协作完成的,在这种情况下,各个对象内部是如何实现自己的,对系统设计人员来说就不是那么重要了

而各个对象之间的协作关系则成为系统设计的关键,小到不同类的通信,大到各模块之间的交互,在系统设计之初都是要着重考虑的,这也是系统设计的主要工作内容. 面向接口编程就是指按照这种思想来编程. 



**关于接口的理解**

1. 接口从更深层次的理解,应是定义(规范,约束)与实现(名实分离的原则)的分离.
2. 接口的本身反映了系统设计人员对系统的抽象理解
3. 接口应有两类:
   1. 第一类是对一个个体的抽象,它可对应为一个抽象体(abstract class);
   2. 第二类是对一个个体某一方面的抽象,形成一个抽象面(interface);
4. 一个个体有可能是有多个抽象面,抽象面和抽象体是有区别的

**三个面向的区别**

- 面向对象是指,我们考虑问题的时候,以对象为单位,考虑他的属性及方法
- 面向过程是指,我们考虑问题时,以一个具体的流程(事务过程)为单位,考虑它的实现
- 接口设计与非接口设计是针对复用技术而言,与面向对象(过程)不是一个问题. 更多的体现就是对系统整体的架构
- AOP为Aspect Oriented Programming的缩写，意为：[面向切面编程](https://baike.baidu.com/item/面向切面编程/6016335)，通过[预编译](https://baike.baidu.com/item/预编译/3191547)方式和运行期间动态代理实现程序功能的统一维护的一种技术。AOP是[OOP](https://baike.baidu.com/item/OOP)的延续，是软件开发中的一个热点，也是[Spring](https://baike.baidu.com/item/Spring)框架中的一个重要内容，是[函数式编程](https://baike.baidu.com/item/函数式编程/4035031)的一种衍生范型。利用AOP可以对业务逻辑的各个部分进行隔离，从而使得业务逻辑各部分之间的[耦合度](https://baike.baidu.com/item/耦合度/2603938)降低，提高程序的可重用性，同时提高了开发的效率。

### 2. 使用注解开发(这个注解是指mybatis的注解,不是spring的注解)

**使用注解开发之后,我们就舍弃了一些不必要的配置**

![image-20200724130540908](G:\JYGS\dir\programming_experience\编程心得\mybatis.assets\image-20200724130540908.png)





![image-20200724130607409](mybatis.assets\image-20200724130607409.png)

**虽然不要绑定配置文件,但是需要绑定接口**

```xml
<!-- 绑定接口 -->
    <mappers>
        <mapper class="com.grox.mapper.UserDao"/>
    </mappers>
```

```java
// 使用注解查询全部
    @Select("select * from user")
    List<User> getAll();
```

```java
/**
     * 使用注解查询全部
     */
    @Test
    public void getAll() {
        SqlSession sqlSession = MybatisUtils.getSqlSession();
        UserDao mapper = sqlSession.getMapper(UserDao.class);
        List<User> all = mapper.getAll();
        for (User user : all) {
            System.out.println("user = " + user);
        }
        sqlSession.close();
    }
```



本质: 反射机制实现

底层: 动态代理

![image-20200724132533604](mybatis.assets/image-20200724132533604.png)



![image-20200724132533604](mybatis.assets/111.png)





**mybatis运行底层**



![image-20200727100907596](mybatis.assets\image-20200727100907596.png)

> 创建sqlsessionFactory的构造器(建造者模式),用完后立刻丢弃,释放内存资源,可以重用SqlSessionFactoryBuilder来构建多个SqlSessionFactory实例,调用,build方法,将资源的流对象传进去



![image-20200727102247545](mybatis.assets\image-20200727102247545.png)

> 解析完之后传回给configuration对象

![image-20200727102539408](mybatis.assets\image-20200727102539408.png)



### 3. 注解实现CRUD

我们可以在工具类创建的时候实现自动提交事务

```java
public static SqlSession getSqlSession() {
        // 在openSession中添加boolean值来开启是否自动提交,默认为false
        return sqlSessionFactory.openSession(true);
    }
```



**具体操作请根据8.2自行书写,本文档不在一一举例**



**关于@Param()注解**

+ 基本类型的参数或者String类型,需要加上
+ 引用类型不需要加
+ 如果只有一个基本类型,可以忽略,建议加上
+ 我们在SQL中引用的就是@Param()中设定的属性名



**延伸知识点:**

**#{} 和${} 的区别**

> 两者都可以在mybatis中用在输入映射
>
> "#{}" 是预编译处理,起到占位符的作用(等同于preparedStatement)
>
> ${}是字符串替换。(等同于Statement)
>
> mybatis在处理#{}时，会将sql中的#{}替换为?号，调用PreparedStatement的set方法来赋值；
> mybatis在处理 $ { } 时，就是把 ${ } 替换成变量的值，完成的是简单的字符串拼接。
>
> 补充：在mybatis中使用#{}可以防止sql注入[^2]，提高系统安全性 ;  但是Mybaties排序时使用order by 动态参数时需要注意，使用${}而不用#{}.



## 9. Lombok

使用步骤:

1. 在Idea中安装那个Lombok插件!

2. 在项目中导入lombok的jar包

   ```xml
   <dependency>
   	<groupId>org.projectlombok</groupId>
       <artifactId>lombok</artifactId>
       <version>1.8.15</version>
   </dependency>
   ```

3. 在实体类上加注解

==极其不建议使用==

**Lombok的优缺点**

优点: 

	1. 能通过注解的形式自动生成构造器、getter/setter、equals等方法，提高了一定的开发效率。
 	2. 让代码变得整洁，不用过多的去关注相应的方法
 	3. 竖向做修改的时候，也简化的维护为这些属性方法的时间成本。

缺点：

1. 不支持多种参数构造器的重载
2. 虽然省去了手动建造getter/setter方法的时间，但是大大降低了源代码的可读性和完整性，增加了阅读源代码的难度。不利用新手查看。

**总结**

​	Lombok虽说有很多优点，但是Lombok跟类似于一个IDE插件，项目也需要依赖相应的jar包。Lombok依赖jar包是因为编译时需要用它的注解，为什么说它又类似插件呢？因为在使用时，eclipse和IntelliJ IDEA都需要安装相应和插件，在编译器编译时通过操作AST（抽象语法数）改变字节码生成，变相的就是说它在改变java语法。

​	它不像spring的依赖注入或者mybatis的ORM一样是运行时的特征，而是编译时的特征。最不好的地方就是对插件的依赖，因为Lombok只是省去了一些人工生成代码的麻烦，但IDE都有快捷键来协助生成getter/setter等方法，也非常方便。

知乎上有位大神发表过对Lombok的一些看法

​	<kbd>这是一款低级趣味的对插件,不建议使用.Java发展到今天,各种插件层出不穷,如何甄别各种插件的优劣?能从框架上优化你的设计的,能提高应用程序性能的</kbd>
<kbd>实现高度封装可扩展的...,像Lombok这种,已经不仅仅是插件了,它改变了你如何编写源码,事实上,少去的代码,你写上又如何?如何Java家族到处充斥这样的东西</kbd><kbd>那只不过是一坨披着黄金的屎,迟早会被其他语言取缔的.</kbd>

![image-20200727150957792](mybatis.assets/image-20200727150957792.png)



## 10. 多对一

### 1.按照查询处理(子查询)

```java
@Test
    public void getStudent() {
        SqlSession sqlSession = MybatisUtils.getSqlSession();
        StudentMapper mapper = sqlSession.getMapper(StudentMapper.class);
        List<Student> studentList = mapper.getStudent();
        for (Student student : studentList) {
            System.out.println("student = " + student);
        }
        sqlSession.close();

    }
```

```java
// 写在接口中
List<Student> getStudent();
```

```xml
<resultMap id="studentTecher" type="com.grox.model.Student">
        <result column="id" property="id"/>
        <result column="name" property="name"/>
        <association property="tid" column="tid" select="getTeacher"/>
    </resultMap>

    <select id="getStudent" resultMap="studentTecher">
        select * from student
    </select>

    <select id="getTeacher" resultType="com.grox.model.Teacher">
        select name from teacher where id = #{id}
    </select>
```

### 2. 结果集映射

```java
@Test
    public void getStudent2() {
        SqlSession sqlSession = MybatisUtils.getSqlSession();
        StudentMapper mapper = sqlSession.getMapper(StudentMapper.class);
        List<Student> studentList = mapper.getStudent2();
        for (Student student : studentList) {
            System.out.println("student = " + student);
        }
        sqlSession.close();

    }
```

```java
 List<Student> getStudent2();
```

```xml
<!--按照结果嵌套处理-->
    <select id="getStudent2" resultMap="studentTecher2">
        select s.id sid, s.name sname,t.name tname
        from student s, teacher t 
        where s.tid = t.id
    </select>
    <resultMap id="studentTecher2" type="com.grox.model.Student">
        <result property="id" column="sid"/>
        <result property="name" column="sname"/>
        <association property="tid" javaType="com.grox.model.Teacher">
            <result property="name" column="tname"/>
        </association>
    </resultMap>
```

```xml
<!--关联,用于对象-->
<association> </association>  
<!--集合,用于集合-->
<collection> </collection> 
```

## 11. 一对多

## 12. 动态sql

**动态SQL就是指根据不同的条件生成不同的SQL语句**

> UUID生成的方法  调用java.util.UUID类的randomUUID方法获取UUID

```java
import.util.UUID
UUID.rondomUUID().toString().replaceAll("-", "");
```

==具体运用请自行去mybatis的中文社区学习,[mybatis中文社区](https://mybatis.org/mybatis-3/)==

## 13.  缓存

### 1、简介

1. 什么是缓存(Cache)
   + 存在内存中的临时数据。
   + 将用户经常查询的数据放在缓存（内存）中，用户去查询数据就不用充磁盘上（关系型数据库数据文件）查询，从缓存中查询，从而提高查询效率，结局高并发系统的性能问题
2. 为什么使用缓存？

   + 减少和数据库的交互次数，减少系统开销，提高系统效率。
3. 什么样的数据可以使用缓存？
   + 经常查询并且不经常改变的数据。

### 2、Mybatis缓存

+ Mybatis包含一个非常强大的查询缓存特性，他可以非常方便的定制和配置缓存。缓存可以极大的提升查询效率。
+ Mybatis系统中默认定义了两极缓存：一级缓存和二级缓存
  + 默认情况下，只有一级缓存里开启。（SqlSession级别的缓存，也称为本地缓存）
  + 二级缓存需要手动开启和配置，它是基于namespace级别的缓存。
  + 为了提高扩展性，Mybatis定义了缓存接口Cache，我们可以通过实现Cache接口来定义二级缓存。



### 3、一级缓存

+ 一级缓存又叫本地缓存：
  + 与数据同一次会话期间查询到的数据会放在本地缓存中。
  + 以后如果需要获取相同的数据，直接从缓存中拿，没必要再去查询数据库；

缓存失效的情况：

1. 查询不同的东西

2. 增删改操作，可能会改变原来的数据，所以必定会刷新缓存。

3. 查询不同的mapper.xml

4. 手动清理缓存
   1. sqlSession.clearCache()



小结：一级缓存是默认开启的，只在一次会话中有效，也就是拿到链接到关闭链接的区间。	

### 4、二级缓存

+ 二级缓存又叫全局缓存，一级缓存的作用域太低了，所以诞生了二级缓存
+ 基于namespace级别的缓存，一个名称空间，对应一个二级缓存；
+ 工作机制
  + 一个会话查询一条数据，这个数据就会被放在当前会话的一级缓存中；
  + 如果当前会话关闭了，这个会话对应的一级缓存就会消失；但是我们想让他在会话关闭之后，一级缓存中的数据被保存到二级缓存中去
  + 新的会话查询信息，就可以从二级缓存中获取内容。
  + 不同的mapper查询出来的数据会放在自己对应的缓存中。

步骤：

1. 开启全局缓存

   ```xml
    <settings>
           <setting name="logImpl" value="STDOUT_LOGGING"/>
        	<!--开启缓存-->
           <setting name="cacheEnabled" value="true"/>
       </settings>
   ```

2. 使用二级缓存

   ```xml
   <cache/>
   ```

   

3. 测试

4. 小结:

   + 只要开启了二级缓存,在同一个Mapperxia就有效

   + 所有数据都会先放在一级缓存中

   + 只有当会话提交,或者关闭的时候,才会提交到二级缓存中!

### 5、缓存原理



![image-20200730155957052](mybatis.assets/image-20200730155957052.png)



### 6、自定义缓存--ehcache

Ehcache是一种广泛使用的开源Java分布式缓存,主要面向通用缓存

```xml
<dependency>
            <groupId>org.mybatis.caches</groupId>
            <artifactId>mybatis-ehcache</artifactId>
            <version>1.1.0</version>
        </dependency>
```





































































































































































 

[^1]: 对象关系映射（Object Relational Mapping，简称ORM）是通过使用描述对象和数据库之间映射的[元数据](https://baike.baidu.com/item/元数据/1946090)，将面向对象语言程序中的对象自动持久化到关系数据库中。本质上就是将数据从一种形式转换到另外一种形式。 这也同时暗示着额外的执行开销；然而，如果ORM作为一种中间件实现，则会有很多机会做优化，而这些在手写的持久层并不存在。 更重要的是用于控制转换的元数据需要提供和管理；但是同样，这些花费要比维护手写的方案要少；而且就算是遵守ODMG规范的对象数据库依然需要类级别的元数据。

[^2]: SQL注入即是指web应用程序对用户输入数据的合法性没有判断或过滤不严，攻击者可以在web应用程序中事先定义好的查询语句的结尾上添加额外的SQL语句，在管理员不知情的情况下实现非法操作，以此来实现欺骗数据库服务器执行非授权的任意查询，从而进一步得到相应的数据信息

## 一个Spring上的邪术,抑制警告@SuppressWarnings

![image-20200728140359789](mybatis.assets/image-20200728140359789.png)