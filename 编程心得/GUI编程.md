# GUI编程

+ 什么是GUI
+ 怎么玩
+ 我们平时怎么用

组件

+ 窗口
+ 弹窗
+ 面板
+ 文本框
+ 列表框
+ 按钮
+ 图片
+ 监听事件
+ 鼠标
+ 键盘事件
+ 破解工具

## 1. 简介

Gui的核心技术 : Swing Awt

不流行的原因

+ 界面不美观
+ 需要下载jre,成本大

## 2. AWT

### 2.1. AWT介绍

AWT(抽象的窗口工具)

1. 包含了很多类和接口  GUI
2. 元素: 窗口, 按钮, 文本框
3. java.awt

![image-20200821095715061](C:\Users\Grox\AppData\Roaming\Typora\typora-user-images\image-20200821095715061.png)



### 2.2. 组件和容器

#### 2.2.1 Frame

```java
public class TestFrame {
    public static void main(String[] args) {
        // 获取界面对象
        Frame frame = new Frame("文件转换");
        // 设置可见性
        frame.setVisible(true);
        // 设置窗口大小
        frame.setSize(600,600);
        // 设置背景颜色
        frame.setBackground(Color.WHITE);
        // 弹出的初始位置
        frame.setLocation(650,200);
        // 设置大小固定
        frame.setResizable(false);
    }
}
```

#### 2.2.2 面板Panel

```java
public class TestFramePanel {
    public static void main(String[] args) {
        Frame frame = new Frame("文件转换");

        Panel panel = new Panel();
        // 设置布局
        frame.setLayout(null);
        // 坐标
        frame.setBounds(300, 300, 500, 500);
        frame.setBackground(Color.WHITE);

        // panel设置坐标,相对于frame
        panel.setBounds(10, 30, 50, 25);
        panel.setBackground(Color.cyan);

        // 添加
        frame.add(panel);
        frame.setVisible(true);

        // 设置大小固定
        frame.setResizable(false);


        // 监听事件,经停窗口关闭事件 System.exit(0) 
        // 适配器模式
        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });
    }
}

```

#### 2.2.3 布局器管理

+ 流式布局

```java
// 设置布局
// 流式布局
frame.setLayout(new FlowLayout(FlowLayout.LEFT));
```

+ 东西南北中

```java
// 东西南北中布局
        Button button1 = new Button("east");
        Button button2 = new Button("west");
        Button button3 = new Button("north");
        Button button4 = new Button("south");
        Button button5 = new Button("center");

        frame.add(button1, BorderLayout.EAST);
        frame.add(button2, BorderLayout.WEST);
        frame.add(button3,BorderLayout.NORTH);
        frame.add(button4, BorderLayout.SOUTH);
        frame.add(button5, BorderLayout.CENTER);

```

+ 表格布局

```java
        // 表格布局
        Button button1 = new Button("east");
        Button button2 = new Button("west");
        Button button3 = new Button("north");
        Button button4 = new Button("south");
        Button button5 = new Button("center");
        Button button6 = new Button("center");

        frame.setLayout(new GridLayout(3,2));
        frame.add(button1);
        frame.add(button2);
        frame.add(button3);
        frame.add(button4);
        frame.add(button5);
        frame.add(button6);

        frame.pack(); // java函数,自动选择最优设计(包括表格的宽度,自动匹配文字)
```



**小练习**

![image-20200824093555345](GUI编程.assets/image-20200824093555345.png)

```java
public class DemoFrame {
    public static void main(String[] args) {
        Frame frame = new Frame("测试");
        frame.setBounds(300, 400, 400, 300);
        frame.setBackground(Color.WHITE);

        // 设置一个两行一列的表格
        frame.setLayout(new GridLayout(2, 1));
        // 创建4个面板
        Panel p1 = new Panel(new BorderLayout());
        Panel p2 = new Panel(new GridLayout(2, 1));
        Panel p3 = new Panel(new BorderLayout());
        Panel p4 = new Panel(new GridLayout(2, 2));

        // 设置第一大行的左右板块
        p1.add(new Button("left-1"), BorderLayout.EAST);
        p1.add(new Button("right-1"), BorderLayout.WEST);
        // 设置第一大行的中间板块的上下模块
        p2.add(new Button("p1-top-1"));
        p2.add(new Button("p1-top-2"));
        p1.add(p2, BorderLayout.CENTER);

        p3.add(new Button("left-2"), BorderLayout.EAST);
        p3.add(new Button("right-2"), BorderLayout.WEST);

        for (int i = 0; i < 4; i++) {
            p4.add(new Button("for-" + i));
        }
        p3.add(p4, BorderLayout.CENTER);

        frame.add(p1);
        frame.add(p3);

        // 设置可见性
        frame.setVisible(true);

        // 设置大小固定
        //frame.setResizable(false);


        // 监听事件,经停窗口关闭事件 System.exit(0)
        // 适配器模式
        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });
    }
}
```

总结

1. Frame是一个顶级窗口
2. Panel无法单独显示,必须添加到某个容器中
3. 布局管理器
   1. 流式
   2. 东西南北中
   3. 表格
4. 大小,定位,背景颜色,可见性,监听!



## 3. Swing