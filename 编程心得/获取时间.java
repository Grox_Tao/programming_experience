public class GetDate {
    /**
     * 把时间戳转化为年月日格式
     *
     * @param stime
     * @return
     */
    public static Date getSDate(String stime) {
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
        Long s = Long.valueOf(stime);
        Date date = new Date(s);
        String format = sf.format(date);
        try {
            return sf.parse(format);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 获取传入日期的明天
     *
     * @param date
     * @return
     */
    public static Date getMingtian(Date date) {
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        calendar.add(Calendar.DATE, 1);
        Date time = calendar.getTime();
        return time;
    }
}