



# 初识Mysql

JavEE: 企业级java开发,Web

前端(页面:展示数据)

后台(连接点: 链接数据库jdbc,链接前端(控制,控制视图跳转,和给前端传递数据))

---

## 1.1 什么是数据库

数据库 (DB DataBase)

概念: 数据仓库,软件,安装在操作系统(window,linux)上

作用: 存储数据, 管理数据.

## 1.2 数据库分类

**关系型数据库(Sql)**

+ Mysql,  Oracle,  Sql Server,  DB2,  SQLlite
+ 通过表和表之间,  行和列之间的关系进行数据的存储, 

**非关系型数据库(Nosql)**

+ Redis,  MongDB
+ 非关系型数据库是对象存储,  通过对象的自身的属性来确定

## 安装教程

[Mysql安装教程](https://blog.csdn.net/qq_33369905/article/details/105828923)

> 尽量不要使用exe来安装mysql,exe不仅仅安装了软件,还有很多注册表,如果你只卸载软件没有删除注册表的话,就无法再次安装mysql.

1、解压

2、把包放在自己的环境目录下

3、配置环境变量

4、创建mysql配置文件my.ini

```sql
[mysqld]
basedir=[mysql路径]\mysql-5.7.19\
datadir=[mysql路径]\mysql-5.7.19\data\
port=3306
# 免密
skip-grent-tables
```

5、启动管理员模式下的cmd

6、安装mysql服务

7、初始化数据库文件

8、启动mysql，进去修改密码

9、重启mysql，删除配置文件中的免密代码

## 数据库列类型

> 数值型

| tintint   | 十分小的数据       | 1个字节 |
| --------- | ------------------ | ------- |
| smallint  | 较小的数据         | 2个字节 |
| mediumint | 中等大小的数据     | 3个字节 |
| int       | 标准的数据         | 4个字节 |
| bigint    | 较大的数据         | 8个字节 |
| float     | 浮点数(单精度)     | 4个字节 |
| double    | 浮点数(双精度)     | 8个字节 |
| decimal   | 字符串形式的浮点数 |         |

> 字符型

| char     | 字符串固定大小       | 0~255                         |
| -------- | -------------------- | ----------------------------- |
| varchar  | 可变字符串           | 0~65535(相当于java中的String) |
| tinytext | 微型文本             | 2^8-1                         |
| text     | 文本串(保存较大文本) | 2^16-1                        |

> 时间日期

| date      | yyyy-MM-dd                  |
| --------- | --------------------------- |
| time      | HH:mm:ss                    |
| datetime  | yyyy-MM-dd HH:mm:ss         |
| timestamp | 时间戳,从1970到今天的毫秒数 |
| year      | 年份                        |

## 数据库的字段属性

==Unsigned==

+ 无符号的整数
+ 声明了该列不能声明为负数

==zerofill==

+ 零填充的
+ 不足的位数,使用零来填充,  int(3),5 ---> 005

==自增==

+ 通常理解为自增,自动在上一条记录的基础上+1 (默认)
+ 通常用来设计唯一的主键,index,必须是整数类型
+ 可以自定义设计主键自增的起始何步长

==非空==

+ 假设设置为not null ,如果不给他赋值就会报错
+ null 如果不填写值,默认就是null

==默认==

+ 设置默认的值
+ sex 默认值是男,如果不指定该列的值.则会有默认值

## 数据表的类型

|            | MYISAM | INNODB       |
| ---------- | ------ | ------------ |
| 事务支持   | 不支持 | 支持         |
| 数据行锁定 | 不支持 | 支持         |
| 外键约束   | 不支持 | 支持         |
| 全文索引   | 支持   | 不支持       |
| 表空间大小 | 较小   | 较大,约为2倍 |

常规使用操作

+ myisam  节约空间,速度较快
+ innodb  安全性高,事务的处理,多表多用户操作

## 事务

==要么都成功,要么都不成功==

将一组SQL放在一个批次中执行

### 事务原则

> ACID原则

**原子性(Atomicity):  原子性是值事务是一个不可分割的工作单位,事务中的操作要么都发生,要么都不发生||一个过程的两个步骤要么一起成功,要么一起失败,不能只发生一个动作.(银行转账)**

**一致性(Consistency):  事务前后数据的完整性必须保持一致||表示事务完成后,符合逻辑运算**

**隔离性(Isolation):  是指多个用户并发访问数据库时,数据库为每一个用户开启的事务,不能被其他事务的操作数据所干扰,多个并发事务之间要相互隔离||针对过个用户同时操作,主要是排除其他事务对本次事务的影响. 两个事务同时进行,其中一个事务读取到另一个事务还没提交的数据**

**持久性(Durability): 是指一个事务一旦被提交,它对数据库中数据的改变就是永久性的,接下来即使数据库发生故障也不应该对其他有任何影响||表示事务结束后的数据不会随着外界原因导致数据流失(持久到硬盘)**

+ 假设在使用数据库时断电
  + 事务未提交: 恢复到原状(上一次提交的时候)
  + 事务已提交:持久化到数据库

==事务一旦提交是不可逆的==

### 事务的隔离级别

> 隔离导致的一些问题

**脏读: 一个事务读取了另一个事务没有提交的数据**

**幻读(虚读): 指在一个事务内读取到了别的事务插入的数据,导致前后读取不一致**

**不可重复读: 在一个事务内读取某一行数据时,多次读取结果不同(这个不一定是错误,只是场合不对)**

---------------------------------------------------------------

-- mysql默认开启事务自动提交

事务流程

```sql
-- 手动处理事务
set autocomit = 0 -- 关闭自动提交
-- 事务开启
start transaction -- 标记一个事务的开始,从这个之后的sql都在同一个事务里
-- 提交 :持久化(成功)
commit
-- 回滚 :恢复原来的样子(失败)
rollback
-- 事务结束
set autocommit = 1 -- 开启自动提交
```



## 索引

[索引](http://blog.codinglabs.org/articles/theory-of-mysql-index.html)

> Mysql官方对于索引的定义为:**索引(index)是帮助mysql高效获取数据的数据结构.**
>
> 提取句子主干,就可以得到索引的本质: 索引是数据结构

### 索引的分类

> 在一个表中,主键索引只能有一个,唯一索引可以有多个

+ 主键索引(PRIMARY KEY)
  + 唯一的标识,主键不可重复,只有一个列作为主键
+ 唯一索引(UNQUE KEY)
  + 避免重复的列出现,唯一索引可以重复,多个列都可以标识唯一索引
+ 常规索引(KEY/INDEX)
+ 全文索引(FullText)

### 索引原则

+ 索引不是越多越好
+ 不要对进程变动数据加索引
+ 小数据量的表不要加索引
+ 索引一般加在常用来查询的字段上

## 规范数据库设计

### 为什么需要设计数据库

==当数据库比较复杂的时候,我们就需要设计数据库==

**糟糕的数据库设计**

+ 数据冗余,浪费空间
+ 频繁使用物理外键,数据库插入和删除都很麻烦,甚至会产生异常
+ 程序的性能差

**良好的数据库设计**

+ 节省内存空间
+ 保障数据库的完整性(摒弃物理外键,除非业务需求)
+ 方便系统开发

## 三大范式

**为什么要数据规范化**

+ 避免信息重复
+ 避免更新异常
+ 避免插入异常
  + 无法正常显示信息
+ 删除异常
  + 丢失有效信息



> 三大范式 (规范数据库)

**第一范式(1NF)**

==要求数据库表的每一列都是不可分割的原子数据项==

**第二范式(2NF)**

前提: 满足第一范式

==第二范式需要确保数据库表中每一列都和主键相关,而不能只与主键的某一部分相关(主要针对联合主键)==

**第三范式(3NF)**

前提: 满足第一范式和第二范式

==第三范式需要确保数据表中每一列数据都和主键直接相关,而非间接相关(在2NF基础上消除依赖传递)==



### 规范性与性能的问题

三大范式用于规范数据库,但是鱼和熊掌不可兼得,数据库规范性越高,相应的数据库的性能就会差,于是出于性能和用户体验角度,不得不违背一些规范性

+ 关联查询的表不得超过三张
+ 考虑商业化的需求和目标(成本, 用户体验) 数据库的性能更重要
+ 在规范性能的问题的时候,需要适当的考虑一下规范性
+ 故意给某些表一些冗余字段(从多表查询变为单表查询,提高性能)
+ 故意增加一些计算列 ( 从大数据量降低为小数据的查询 )



## JDBC

### 数据库驱动

![数据库驱动](G:\JYGS\dir\programming_experience\pic\数据库驱动.png)



### JDBC

我们的程序会通过数据库驱动,和数据库打交道

但是,每个数据库都有其单独的驱动,不利于我们程序的管理

于是sun公司 为了简化开发人员的对于数据库的操作,提供可一个Java操作数据库的规范Java Database Connectivity  俗称JDBC

![JDBC](G:\JYGS\dir\programming_experience\pic\JDBC.png)

==在程序中,没有什么是加一层解决不了的.如果有,再加一层==

useUnicaode = true(支持中文编码)

characterEncoding=utf8(设置字符集)

useSSL=true(使用安全的链接)

### sql注入问题

#### 什么是sql注入

sql存在漏洞,会被攻击导致数据泄露

```java
// 此代码需事先有jdbcutils工具类
public class sqlzhuru {
    public static void main(String[] args){
        // login("user","123456");  -- 正常输入
        // login(" 'or '1=1 ","123456");  -- sql注入输入
    }
    
    // 登录业务
    public static void login(String username,String password){
        Connection conn = null;
        Statement st = null;
        ResultSet rs = null;
        try{
            conn = JdbcUtils.getConnect();
            st = conn.createStatement();
            
            // select * from users where `name` = 'user'and `password` = '123456'" -- 正常
            //select * from users where `name` = ' 'or '1=1 'and `password` = ' 123456 '" --sql注入
            String sql = "select * from users where `name` = ' " + username + " 'and `password` = '  " + password + " '";
            rs = st.exrcuteQuery(sql);
            while(rs.next){
                System.out.println(rs.getString("name"));
                System.out.println(rs.getString("password"));
                System.out.println(rs.getString("====================="));
            }
        } catch(SQLException e){
            e.printStackTrace();
        } finally{
            JdbcUtils.release(conn,st.rs);
        }
    }
}
```

> 防止sql注入  PreparedStatement
>
> 本质: 把传递过来的参数当做字符,假设其中存在转义字符,比如说 '' ,PreparedStatement会将它直接转义

## 数据库连接池

> 数据库的链接过程

数据库链接--->执行sql---->执行完毕--->释放资源

而链接--->释放的过程是十分消耗系统资源的

**池化技术:  准备一些预先的资源,过来就链接预先准备好的**