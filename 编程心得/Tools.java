public class Tool {
    // 根据所传列表集合和列表字段获取列号
    public Integer getKey(Map<Integer, String> map, String value) {
        Integer Key = null;
        Set<Integer> integers = map.keySet();
        for (Integer integer : integers) {
            if (map.get(integer).equals(value)) {
                Key = integer;
            }
        }
        return Key;
    }

    // 根据所传集合判断两个集合谁的size更大
    public int getSize (List a, List b) {
        return Math.max(a.size(), b.size());
    }
}