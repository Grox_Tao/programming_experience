package com.sgcc.sgga.common;
/*
 * @ClassName ImagetServlet
 * @CNname  实现验证码功能
 * @Author Grox
 * @Date 2021/5/28 16:43 周五
 * @Version 1.0
 */

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Random;

public class ImagetServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // 页面每3秒刷新
        resp.setHeader("refresh", "3");
        // 在内存中设置一个图片
        BufferedImage image = new BufferedImage(80, 80, BufferedImage.TYPE_INT_RGB);
        // 得到图片
        Graphics2D graphics = (Graphics2D) image.getGraphics();
        // 设置背景颜色
        graphics.setBackground(Color.white);
        // 设置背板大小
        graphics.fillRect(0, 0, 80, 80);
        // 插入数据
        // 设置文字颜色
        graphics.setColor(Color.BLUE);
        // 设置字体
        graphics.setFont(new Font(null, Font.BOLD, 20));
        // 画出数据
        graphics.drawString(getRandom(), 0, 80);

        // 告诉浏览器,这个请求用图片的格式打开
        resp.setContentType("image/jpeg");
        // 不让浏览器缓存
        resp.setDateHeader("expires", -1);
        // 把图片写出
        ImageIO.write(image, "jpeg", resp.getOutputStream());
    }

    private String getRandom() {
        Random random = new Random();
        String r = random.nextInt(99999999) + "";
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < 7 - r.length(); i++) {
            sb.append("0");
        }
        String s = sb.toString() + r;
        return s;
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
