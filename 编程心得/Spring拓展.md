# Spring拓展

不但支持自己定义的@Autowired注解，还支持几个由JSR-250规范定义的注解，它们分别是@Resource、@PostConstruct以及@PreDestroy。

　　**@Resource**的作用相当于@Autowired，只不过@Autowired按byType自动注入，而@Resource默认按 byName自动注入罢了。@Resource有两个属性是比较重要的，分是name和type，Spring将@Resource注解的name属性解析为bean的名字，而type属性则解析为bean的类型。所以如果使用name属性，则使用byName的自动注入策略，而使用type属性时则使用byType自动注入策略。如果既不指定name也不指定type属性，这时将通过反射机制使用byName自动注入策略。
　　

> @Resource装配顺序
> 　　1. 如果同时指定了name和type，则从Spring上下文中找到唯一匹配的bean进行装配，找不到则抛出异常
> 　　2. 如果指定了name，则从上下文中查找名称（id）匹配的bean进行装配，找不到则抛出异常
> 　　3. 如果指定了type，则从上下文中找到类型匹配的唯一bean进行装配，找不到或者找到多个，都会抛出异常
> 　　4. 如果既没有指定name，又没有指定type，则自动按照byName方式进行装配；如果没有匹配，则回退为一个原始类型进行匹配，如果匹配则自动装配；



> @Autowired 与@Resource的区别： 
>
> 1、 @Autowired与@Resource都可以用来装配bean. 都可以写在字段上,或写在setter方法上。
>
> 2、 @Autowired默认按类型装配（这个注解是属业spring的），默认情况下必须要求依赖对象必须存在，如果要允许null值，可以设置它的required属性为false，如：@Autowired(required=false) ，如果我们想使用名称装配可以结合@Qualifier注解进行使用，如下：

```
@Autowired()
@Qualifier("baseDao")
private BaseDao baseDao;
```

> 3、@Resource（这个注解属于J2EE的），默认按照名称进行装配，名称可以通过name属性进行指定，如果没有指定name属性，当注解写在字段上时，默认取字段名进行安装名称查找，如果注解写在setter方法上默认取属性名进行装配。当找不到与名称匹配的bean时才按照类型进行装配。但是需要注意的是，如果name属性一旦指定，就只会按照名称进行装配。

```
@Resource(name="baseDao")
private BaseDao baseDao;
```

推荐使用：@Resource注解在字段上，这样就不用写setter方法了，并且这个注解是属于J2EE的，减少了与spring的耦合。这样代码看起就比较优雅。

 

@Autowired是根据类型进行自动装配的。如果当Spring上下文中存在不止一个UserDao类型的bean时，就会抛出BeanCreationException异常;如果Spring上下文中不存在UserDao类型的bean，也会抛出BeanCreationException异常。我们可以使用@Qualifier配合@Autowired来解决这些问题。如下：

①可能存在多个UserDao实例

 ```java
@Autowired   
@Qualifier("userServiceImpl")   
public IUserService userService;  
 ```

或者

```java
@Autowired   
public void setUserDao(@Qualifier("userDao") UserDao userDao) {   
    this.userDao = userDao;   
}  
```

这样Spring会找到id为userServiceImpl和userDao的bean进行装配。

 

②可能不存在UserDao实例

 ```java
@Autowired(required = false)   
public IUserService userService  
 ```



 

个人总结：

@Autowired//默认按type注入
@Qualifier("cusInfoService")//一般作为@Autowired()的修饰用
@Resource(name="cusInfoService")//默认按name注入，可以通过name和type属性进行选择性注入

 

简单理解：

@Autowired 根据类型注入， 

@Resource 默认根据名字注入，其次按照类型搜索

@Autowired @Qualifier("userService") 两个结合起来可以根据名字和类型注入



一般@Autowired和@Qualifier一起用，@Resource单独用。

当然没有冲突的话@Autowired也可以单独用

 

 

-----------常用注解--------

 

--定义Bean的注解

 

@Controller

@Controller("Bean的名称")

定义控制层Bean,如Action

 

@Service      

@Service("Bean的名称")

定义业务层Bean

 

@Repository  

@Repository("Bean的名称")

定义DAO层Bean

 

@Component  

定义Bean, 不好归类时使用.

 

--自动装配Bean （选用一种注解就可以）

@Autowired  (Srping提供的)

默认按类型匹配,自动装配(Srping提供的)，可以写在成员属性上,或写在setter方法上

 

@Autowired(required=true)  

一定要找到匹配的Bean，否则抛异常。 默认值就是true 

 

@Autowired

@Qualifier("bean的名字") 

按名称装配Bean,与@Autowired组合使用，解决按类型匹配找到多个Bean问题。

 

@Resource  JSR-250提供的

默认按名称装配,当找不到名称匹配的bean再按类型装配.

可以写在成员属性上,或写在setter方法上

可以通过@Resource(name="beanName") 指定被注入的bean的名称, 要是未指定name属性, 默认使用成员属性的变量名,一般不用写name属性.

@Resource(name="beanName")指定了name属性,按名称注入但没找到bean, 就不会再按类型装配了.

 

@Inject  是JSR-330提供的

按类型装配，功能比@Autowired少，没有使用的必要。

 

--定义Bean的作用域和生命过程

@Scope("prototype")

值有:singleton,prototype,session,request,session,globalSession

 

@PostConstruct 

相当于init-method,使用在方法上，当Bean初始化时执行。

 

@PreDestroy 

相当于destory-method，使用在方法上，当Bean销毁时执行。

 

--声明式事务

@Transactional  

@Autowired @Resource @Qualifier的区别

实用理解：@Autowired @Resource 二选其一，看中哪个就用哪个。

 



 