



# 初识MySQL

## 为什么学习数据库

1、岗位技能需求

2、现在的世界,得数据者得天下

3、存储数据的方法

4、程序,网站中,大量数据如何长久保存?

5、数据库是几乎软件体系中最核心的一个存在。

## 什么是数据库

数据库 ( DataBase , 简称DB )

概念 : 长期存放在计算机内,有组织,可共享的大量数据的集合,是一个数据 "仓库"

作用 : 保存,并能安全管理数据(如:增删改查等),减少冗余...

### 数据库总览 :

关系型数据库 ( SQL )

MySQL , Oracle , SQL Server , SQLite , DB2 , ...

关系型数据库通过外键关联来建立表与表之间的关系

### 非关系型数据库 ( NOSQL )

Redis , MongoDB , ...

非关系型数据库通常指数据以对象的形式存储在数据库中，而对象之间的关系通过每个对象自身的属性来决定

## 什么是DBMS

数据库管理系统 ( DataBase Management System )

数据库管理软件 , 科学组织和存储数据 , 高效地获取和维护数据



为什么要说这个呢?

因为我们要学习的MySQL应该算是一个数据库管理系统.

## MySQL简介

概念 : 是现在流行的开源的,免费的 关系型数据库

历史 : 由瑞典MySQL AB 公司开发，目前属于 Oracle 旗下产品。

特点 :

免费 , 开源数据库

小巧 , 功能齐全

使用便捷

可运行于Windows或Linux操作系统

可适用于中小型甚至大型网站应用

官网 : https://www.mysql.com/

## 安装MySQL

这里建议大家使用压缩版,安装快,方便.不复杂.

软件下载

mysql5.7 64位下载地址:

https://dev.mysql.com/get/Downloads/MySQL-5.7/mysql-5.7.19-winx64.zip

电脑是64位的就下载使用64位版本的！

### 安装步骤

1、下载后得到zip压缩包.

2、解压到自己想要安装到的目录，本人解压到的是D:\Environment\mysql-5.7.19

3、添加环境变量：我的电脑->属性->高级->环境变量

> 选择PATH,在其后面添加: 你的mysql 安装文件下面的bin文件夹

4、编辑 my.ini 文件 ,注意替换路径位置

``` sql
[mysqld]
basedir=D:\Program Files\mysql-5.7\
datadir=D:\Program Files\mysql-5.7\data\
port=3306
skip-grant-tables
```

5、启动管理员模式下的CMD，并将路径切换至mysql下的bin目录，然后输入mysqld –install (安装mysql)

6、再输入  mysqld --initialize-insecure --user=mysql 初始化数据文件

7、然后再次启动mysql 然后用命令 mysql –u root –p 进入mysql管理界面（密码可为空）

8、进入界面后更改root密码

``` mysql
update mysql.user set authentication_string=password('123456') where user='root' and Host = 'localhost';
```

9、刷新权限

> flush privileges;

10、修改 my.ini文件删除最后一句skip-grant-tables

11、重启mysql即可正常使用

> net stop mysql
> net start mysql

12、连接上测试出现以下结果就安装好了

![Snipaste_2020-05-22_08-59-47](G:\JYGS\dir\programming_experience\pic\Snipaste_2020-05-22_08-59-47.png)

一步步去做 , 理论上是没有任何问题的 .

如果您以前装过,现在需要重装,一定要将环境清理干净 .

### 几个基本的数据库操作命令 

``` sql
update user set password=password('123456')where user='root'; 修改密码
flush privileges;  刷新数据库
show databases; 显示所有数据库
use dbname；打开某个数据库
show tables; 显示数据库mysql中所有的表
describe user; 显示表mysql数据库中user表的列信息
create database name; 创建数据库
use databasename; 选择数据库

exit; 退出Mysql
? 命令关键词 : 寻求帮助
-- 表示注释
```



