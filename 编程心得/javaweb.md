# Javaweb

![image-20200605152000589](javaweb.assets\image-20200605152000589.png)

## 1 静态web​

+ 静态web存在的缺点
  + web页面无法动态更新,所有用户看到的都是同一个页面
    + 轮播图,点击特效: 伪动态
    + JavaScript[实际开发中,他用的最多]
    + VBScript
  + 他无法和数据库交互(数据无法持久化,用户无法交互)

## 2 动态web

页面会动态展示,显示 的页面因人而异

![image-20200608095449351](javaweb.assets\image-20200608095449351.png)

缺点:

+ 加入服务器的动态web资源出现了错误,我们需要重新编写我们的后台程序,重新发布
  + 停机维护

优点:

+ web页面可以动态更新,所有用户看到的都不是同一个页面
+ 他可以和数据库交互





![image-20200608095723191](javaweb.assets\image-20200608095723191.png)

## 3 Web服务器

ASP

+ 微软出品,国内最早流行的就是ASP
+ 在HTTP中嵌入了VB脚本,ASP+COM
+ 在ASP开发中,基本一个页面都有几千行代码,页面极其混乱
+ 维护成本高
+ 语言多数为C#
+ iis

php: 

+ PHP开发速度很快,功能很强大,跨平台,代码很简单
+ 无法承载大访问量的情况(局限性,适合中小企业的开发)

jsp:

+ sun公司主推的B/S架构
+ 基于java语言号(所有的大公司,或者一些开源的组件,都是用Java写的)
+ 可以承载三高问题带来的影响

### 3.1 服务器

服务器是一种被动的操作,用来处理用户的一些请求和给用户一些响应信息

iis

微软旗下的服务器

#### 3.1.1 tomcat

+ 安装tomcat
  + 在官网中下载符合自己电脑的版本,解压

+ 启动/关闭tomcat
  + 启动 : bin/startup.bat
  + 关闭 : bin/shutdown.bat

访问测试: http://localhost/8080

可能遇见的问题

1. Java环境变量没有配置
2. 闪退问题: 需要兼容配置
3. 乱码问题: 配置文件中配置

==打开 ../config/server.xml(服务器核心配置文件)==

可以在文件中

+ 配置启动的端口号

  + tomcat的默认端口: 8080

  >mysql: 3306
  >
  >http: 80
  >
  >https: 443

```xml
<Connector port="8081" protocol="HTTP/1.1"
               connectionTimeout="20000"
               redirectPort="8443" />
```

+ 可以配置主机和名称
  + 默认的主机名:localhost -->127.0.0.1
  + 默认的网站引用存放位置: wabapps

```xml
<Host name="localhost"  appBase="webapps"
            unpackWARs="true" autoDeploy="true">
```

**面试题**

请谈谈网站是如何进行访问的!

1.输入一个域名,直接回车查询

2.检查本机的C:\Windows\System32\drivers\ect\hosts配置文件下有没有这个域名映射;

	1. 有: 直接返对应和ip地址,这个地址中,有我们需要访问的web程序,可以直接访问

```java
127.0.0.1    www.taoyunpeng.com
```

2. 没有:去DNS服务器找,找到的话就返回,找不到就不返回

![image-20200609144451543](javaweb.assets\image-20200609144451543.png)

## 4 http

### 4.1 什么是http

http (超文本传输协议).是一个简单的请求响应协议,它通常运行在TCP上

### 4.2 两个时代

[http详解](G:\JYGS\dir\programming_experience\编程心得\详解http.md)

+ http1.0
  + http1.0  : 客户端和web服务器连接后,只能获得一个web资源,然后连接便断开了(在一个TCP连接上只有一个http请求响应,如果有其他的http请求,需要一个新的TCP连接)
+ http2.0
  + http1.1  :  客户端和web服务器连接后,可以获得多个web资源,连接持续连接(在一个TCP连接上可以传送多个HTTP请求和响应)
    + 优点: 减少了建立和关闭连接的消耗和延迟。

### 4.3 http请求

+ 客户端---发请求(Request) --- 服务器

百度: 

 ```java
-- General 通用
Request URL: https://www.baidu.com/   请求地址
Request Method: GET  请求方法: get方法
Status Code: 200 OK  状态码: 200
Remote Address: [2408:80f0:410c:1d:0:ff:b07a:39af]:443  远程地址


-- Requeset Headers 请求头
Accept:text/html  告诉浏览器,他所支持的数据类型
Accept-Encoding: gzip, deflate, br  支持哪种编码格式 GBK UTF-8 GB2313 ISO8859-1
Accept-Language: zh-CN,zh;q=0.9,en;q=0.8  告诉浏览器,他的语言环境
Cache-Control: 缓存控制
Connection: keep-alive告诉浏览器,请求完成是断开还是保持连接
 ```

+ 请求方式: Get,Post head,delete put,tract....
  + get: 请求能够携带的参数比较少,大小有限制,会在浏览器的url地址栏显示数据内容,不安全,但高效
  + post: 请求能够携带的参数没有限制,大小没有限制,不会在浏览器url地址栏显示数据内容,安全`但不高效.

### 4.4 http响应

+ 服务器---响应(response) --- 客户端

```java
-- Response Headers 响应头
Cache-Control: private 缓存控制
Connection: keep-alive 连接:保持或者(连接)
Content-Encoding: gzip 内容编码
Content-Type: text/html;charset=utf-8 内容类型
```

#### 响应状态码

200: 请求成功

3**:请求重定向

404: 找不到资源

500: 服务器代码错误

502: 网关错误

**常见面试题**

当你的浏览器中地址栏输入地址并回车的一瞬间到页面能够显示出来,经历了什么?

> 

## 5 maven

**我为什么要学这个技术**

1. 在Javaweb的开发中,需要使用大量的jar包,我们得手动导入;
2. 为了能够让jar包能够自动导入和配置,Maven诞生了;

### 5.1 Maven(项目架构管理工具)

Maven的核心思想: ==约定大于配置==

### 5.2 下载安装Maven

### 5.3 配置环境变量

​	MAVEN_HOME

### 5.4 阿里云镜像

​	国内建议使用[腾讯软件源镜像](https://mirrors.cloud.tencent.com/) 放在conf/setting中

### 5.5 本地仓库

创建一个本地仓库,并将路径放在conf/setting中

## 6 servlet

### 6.1 什么是servlet

![image-20200615173832049](javaweb.assets\image-20200615173832049.png)

+ Servlet就是sun公司开发和动态web的一门技术
+ Sun公司在这些API中提供一个接口叫做:servlet,如果你想开发一个Servlet程序,只要完成两个小步骤:
  + 编写一个类,实现Servlet接口
  + 把开发好的Java类部署到web服务器中

**我们把实现了Servlet接口的Java类叫做Servlet**

```java
// 由于get或者post只是请求实现的不同的方式,可以互相调用,业务逻辑都一样
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("tsxt/html");

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
```



### 6.2 Servlet运行原理

servlet是由web服务器调用,web服务器在收到浏览器请求之后,会

![image-20200612150820114](javaweb.assets\image-20200612150820114.png)

### 6.3 Mapping

1. 一个Servlet可以指定一个映射路径

```xml
<servlet-mapping>
    <servlet-name>hello</servlet-name>
    <url-pattern>/hello</url-pattern>
</servlet-mapping>
```

2. 一个Servlet可以指定多个映射路径

```xml
<servlet-mapping>
    <servlet-name>hello</servlet-name>
    <url-pattern>/hello</url-pattern>
</servlet-mapping>
<servlet-mapping>
    <servlet-name>hello</servlet-name>
    <url-pattern>/hello1</url-pattern>
</servlet-mapping>
<servlet-mapping>
    <servlet-name>hello</servlet-name>
    <url-pattern>/hello2</url-pattern>
</servlet-mapping>
<servlet-mapping>
    <servlet-name>hello</servlet-name>
    <url-pattern>/hello3</url-pattern>
</servlet-mapping>
```

3. 一个Servlet可以指定通用映射路径

   ```xml
   <servlet-mapping>
       <servlet-name>hello</servlet-name>
       <url-pattern>/hello/*</url-pattern>
   </servlet-mapping>
   ```

4.  默认请求路径

   ```xml
   <servlet-mapping>
       <servlet-name>hello</servlet-name>
       <url-pattern>/*</url-pattern>
   </servlet-mapping>
   ```

5.  指定一些后缀或者前缀等等...

   ```xml
   <!-- 可以自定义后缀实请求映射
    	 注意点: *前面不要加项目映射的路径-->
   <servlet-mapping>
       <servlet-name>hello</servlet-name>
       <url-pattern>*.do</url-pattern>
   </servlet-mapping>
   ```

6. 优先级问题

   指定了固有的映射路径优先度最高,如果找不到就会走默认的处理请求;

![image-20200615144617263](javaweb.assets\image-20200615144617263.png)

### 6.4 ServletContext(全局唯一)

Web容器在启动的时候,他会为每个web程序都创建一个对应的ServletContext对象,它代表了当前的web应用;

#### **1、共享数据**

我在这个Servlet中保存的数据,可以在另一个Servlet中拿到

**第一步,向ServletContext放置数据**

```java
@Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //resp.setContentType("text/html");
        //this.getInitParamter() 初始化参数
        //this.getServletConfig() Servlet配置
        //this.getServletContext() Servlet上下文
        ServletContext context = this.getServletContext();
        String username = "Grox";
        context.setAttribute("username",username); //将数据保存在ServletContext中
    }

    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
```

**第二步,从ServletContext拿出数据并输出在浏览器(注意中文编码格式);**

```java
@Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ServletContext context = this.getServletContext();
        String username = (String) context.getAttribute("username"); //将数据从ServletContext中取出
        
        resp.setContentType("text/html");
        resp.setCharaterEncoding("utf-8");
        resp.getWriter().print("名字"+username);
    }

    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
```

**第三步,配置相应的servlet和servletmapping**

```xml
<!-- 将数据保存在ServletContext中 -->
<servlet>
    <servlet-name>setusername</servlet-name>
    <servlet-class>类路径</servlet-class>
</servlet>
<servlet-mapping>
    <servlet-name>setusername</servlet-name>
    <url-pattern>/setusername</url-pattern>
</servlet-mapping>

<!--将数据从ServletContext中取出-->
<servlet>
    <servlet-name>getusername</servlet-name>
    <servlet-class>类路径</servlet-class>
</servlet>
<servlet-mapping>
    <servlet-name>getusername</servlet-name>
    <url-pattern>/getusername</url-pattern>
</servlet-mapping>
```

```java
// ServletContext 常用方法
String getContextPath(); //获取上下文路径
ServletContext getContext(String var1); //获取上下文对象
Set<String> getResourcePaths(String var1);  //获取资源路径
InputStream getResourceAsStream(String var1);  //获取资源取为流
RequestDispatcher getRequestDispatcher(String var1);  //通过请求转发
RequestDispatcher getNamedDispatcher(String var1);  //通过名字转发
```



#### 2、获取初始化参数

```xml
<!--  配置一些web应用的初始化参数  -->
    <context-param>
        <param-name>mysql</param-name>
        <param-value>jdbc:mysql://localhost:3306/mybatis</param-value>
    </context-param>
```

```java
public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ServletContext c = this.getServletContext();
        String mysql = c.getInitParameter("mysql");
		resp.getWriter().print(mysql);
    }
```

#### 3、请求转发

```java
public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ServletContext c = this.getServletContext();
        RequestDispatcher r = c.getRequestDispatcher("/hello"); //请求转发的路径
    	r.forword(req.resp);
    }
```

![image-20200617153807671](javaweb.assets\image-20200617153807671.png)

​																					        ==请求转发==



Properties

+ 在java目录下新建properties

  + 在java目录下回产生配置文件,无法被导出或者无法生效的问题

  ![image-20200617174222015](javaweb.assets\image-20200617174222015.png)

  ```xml
  <!-- 在build中配置resources,来防止资源导出失败的问题 -->
  <build>
      <resources>
          <resource>
          	<directory>src/main/resource</directory>
              <includes>
              	<include>**/*.propreties</include>
                  <include>**/*.xml</include>
              </includes>
              <filtering>true</filtering>
          </resource>
          <resource>
          	<directory>src/main/java</directory>
              <includes>
              	<include>**/*.propreties</include>
                  <include>**/*.xml</include>
              </includes>
              <filtering>true</filtering>
          </resource>
      </resources>
  </build>
  ```

  

+ 在resources目录下新建properties

发现: 都被打包到了同一路径下:classes,我们俗称这个路径为classpath

思路: 需要一个文件流

![image-20200617173907520](javaweb.assets\image-20200617173907520.png)

### 6.5 HttpServletRespone

web服务器接收到客户端的http请求,针对这个请求,分别创建一个代表请求的HttpServletRequest对象和代表响应的一个HttpServletRespone对象;

+ 如果要获取客户端请求过来的参数:找HttpServletRequest
+ 如果要给客户端响应一些:找HttpServletRespone

#### 1 简单分类

负责向浏览器发送数据的方法

```java
ServletOutputStream getOutpuStream() throw IoException;
PrintWriter getWriter() throws IOException;
```

负责向浏览器发送响应头的方法

```java
void setCharacterEncoding(String var1);
void setContentLengh(int var1);
void setCotentLengthLong(long var1);
void setContentType(String var1);
void setDateHeader(String var1, long var2);
void addDateHeader(String var1, long var2);
void setHeader(String var1, String var2);
void addHeader(String var1, String var2);
void setIntHeader(String var1, int var2);
void addIntHeader(String var1, int var2);
```

响应状态码

```java
	int SC_CONTINUE = 100;
    int SC_SWITCHING_PROTOCOLS = 101;
    int SC_OK = 200;
    int SC_CREATED = 201;
    int SC_ACCEPTED = 202;
    int SC_NON_AUTHORITATIVE_INFORMATION = 203;
    int SC_NO_CONTENT = 204;
    int SC_RESET_CONTENT = 205;
    int SC_PARTIAL_CONTENT = 206;
    int SC_MULTIPLE_CHOICES = 300;
    int SC_MOVED_PERMANENTLY = 301;
    int SC_MOVED_TEMPORARILY = 302;
    int SC_FOUND = 302;
    int SC_SEE_OTHER = 303;
    int SC_NOT_MODIFIED = 304;
    int SC_USE_PROXY = 305;
    int SC_TEMPORARY_REDIRECT = 307;
    int SC_BAD_REQUEST = 400;
    int SC_UNAUTHORIZED = 401;
    int SC_PAYMENT_REQUIRED = 402;
    int SC_FORBIDDEN = 403;
    int SC_NOT_FOUND = 404;
    int SC_METHOD_NOT_ALLOWED = 405;
    int SC_NOT_ACCEPTABLE = 406;
    int SC_PROXY_AUTHENTICATION_REQUIRED = 407;
    int SC_REQUEST_TIMEOUT = 408;
    int SC_CONFLICT = 409;
    int SC_GONE = 410;
    int SC_LENGTH_REQUIRED = 411;
    int SC_PRECONDITION_FAILED = 412;
    int SC_REQUEST_ENTITY_TOO_LARGE = 413;
    int SC_REQUEST_URI_TOO_LONG = 414;
    int SC_UNSUPPORTED_MEDIA_TYPE = 415;
    int SC_REQUESTED_RANGE_NOT_SATISFIABLE = 416;
    int SC_EXPECTATION_FAILED = 417;
    int SC_INTERNAL_SERVER_ERROR = 500;
    int SC_NOT_IMPLEMENTED = 501;
    int SC_BAD_GATEWAY = 502;
    int SC_SERVICE_UNAVAILABLE = 503;
    int SC_GATEWAY_TIMEOUT = 504;
    int SC_HTTP_VERSION_NOT_SUPPORTED = 505;
```

#### 2 常见应用

1. 向浏览器输出消息
2. 文件下载
   1. 要获取下载文件的路径
   2. 下载的文件名是什么?
   3. 设置浏览器能够支持下载我们的需要的东西
   4. 获取下载文件的输入流
   5. 创建缓冲区
   6. 获取OutputStream对象
   7. 将FileOutputStream流写入到buffer缓冲区中
   8. 使用OutputStream对象将缓冲区的数据输出到客户端!

```java
public class FileServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //1. 要获取下载文件的路径
        String realPath = "D:\\grox\\MyIdea\\grox\\respone\\src\\main\\resources\\1.png";
        System.out.println("下载文件的路径: " + realPath);
        //2. 下载的文件名是什么?
        String filename = realPath.substring(realPath.lastIndexOf("\\") + 1);
        //3. 设置浏览器能够支持下载我们的需要的东西,URLEncoder.encode可解决文件名中文乱码问题
        resp.setHeader("Content-disposition", "attachment;filename=" + URLEncoder.encode(filename,"utf-8"));
        //4. 获取下载文件的输入流
        FileInputStream in = new FileInputStream(realPath);
        //5. 创建缓冲区
        int len = 0;
        byte[] bytes = new byte[1024];
        //6. 获取OutputStream对象
        ServletOutputStream out = resp.getOutputStream();
        //7. 将FileOutputStream流写入到buffer缓冲区中,使用OutputStream对象将缓冲区的数据输出到客户端!
        while ((len = in.read(bytes)) > 0) {
            out.write(bytes, 0, len);
        }
        out.close();
        in.close();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
```

#### 3 验证码功能

验证怎么来的?

+ 前端实现
+ 后端实现

```java
public class ImageServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // 让浏览器每3秒自动刷新一次
        resp.setHeader("refresh", "3");
        // 在内存中创建一个图片
        BufferedImage image = new BufferedImage(80, 20, BufferedImage.TYPE_INT_RGB);
        // 得到图片
        Graphics2D g = (Graphics2D) image.getGraphics(); //获取笔
        // 设置图片的背景颜色
        g.setColor(Color.white);
        g.fillRect(0, 0, 80, 20);
        // 给图片写数据
        g.setColor(Color.BLUE);
        g.setFont(new Font(null, Font.BOLD, 20));
        g.drawString(makeNum(), 0, 20);

        // 告诉浏览器,这个气流用图片的方式打开
        resp.setContentType("image/png");
        // 网站存在缓存,不让浏览器缓存
        resp.setDateHeader("expirs", -1);
        resp.setHeader("Cache-Control", "no-cache");
        resp.setHeader("Pragma", "no-cache");

        // 把图片写给浏览器
        ImageIO.write(image, "jpg", resp.getOutputStream());
    }

    // 生成验证码
    public String makeNum() {
        Random random = new Random();
        String s = random.nextInt(99999999) + "";
        StringBuffer sb = new StringBuffer();
        System.out.println(s.length());
        for (int i = 0; i < 7 - s.length(); i++) {
            sb.append("0");
        }
        s = sb.toString() + s;
        return s;
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
```

#### 4 实现重定向

![image-20200617154930569](javaweb.assets\image-20200617154930569.png)

​																								==重定向==

```java
resp.sendRedirect(重定向路径);
// 相当于
/*
resp.setHeader("Localtion", "重定向的路径");
resp.setStatus(302);
*/
```



**面试题: 请你聊聊重定向和请求转发的区别**

相同点

+ 页面都会实现跳转

不同点

+ 请求转发,url不会产生变化
+ 重定向,url地址栏会发生变化

### 6.6 HttpServletRequest

HttpServletRequest代表客户端的请求,用户通过http协议访问服务器,HTTP请求中的所有信息会被封装到HttpServletRequest,通过这个HttpServletRequest的方法,获得客户端的所有信息;

![image-20200628175149153](javaweb.assets\image-20200628175149153.png)![image-20200628175155635](javaweb.assets\image-20200628175155635.png)

#### 获取参数,请求转发

```java
public class LoginServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf-8");
        String username = req.getParameter("username");
        String password = req.getParameter("password");
        String[] hobbies = req.getParameterValues("hobbies");
        System.out.println("======================================");
        // 后台接收中文乱码问题
        System.out.println("username = " + username);
        System.out.println("password = " + password);
        System.out.println(Arrays.toString(hobbies));
        System.out.println("======================================");
        // 通过请求转发
        req.getRequestDispatcher(req.getContextPath()+"/success.jsp").forward(req,resp);
        resp.setCharacterEncoding("utf-8");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
```

**面试题: 请你聊聊重定向和请求转发的区别**

相同点

+ 页面都会实现跳转

不同点

+ 请求转发,url不会产生变化  307
+ 重定向,url地址栏会发生变化  302

#### 番外知识

**创建一个maven工程**

1. 新建maven工程(可以选用骨架)

   ![image-20200629164729043](javaweb.assets\image-20200629164729043.png)

2. 书写工作组名称,项目名称和创建位置

![image-20200629164829051](javaweb.assets\image-20200629164829051.png)

3. 确定maven版本,maven配置文件和本地仓库

![image-20200629165005640](javaweb.assets\image-20200629165005640.png)

4. 最后点击Finish完成创建

5. 在main目录下创建源目录java和资源目录resources

   ![image-20200629165302162](javaweb.assets\image-20200629165302162.png)

6. 修改web.xml文件

   ![image-20200629165601495](javaweb.assets\image-20200629165601495.png)

   ```xml
   <web-app version="2.4" xmlns="http://java.sun.com/xml/ns/j2ee"
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xsi:schemaLocation="http://java.sun.com/xml/ns/j2ee
            http://java.sun.com/xml/ns/j2ee/web-app_2_4.xsd">
   
   
   </web-app>
   ============================== 或者
   <?xml version="1.0" encoding="UTF-8" ?>
   <web-app xmlns="http://xmlns.jcp.org/xml/ns/javaee"
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/javaee
                         http://xmlns.jcp.org/xml/ns/javaee/web-app_4_0.xsd"
            version="4.0"
            metadata-complete="true">
   
   
   </web-app>
   ```






## 7 session,cookie

### 7.1、会话

**会话 :**用户打开一个浏览器,点击了很多超链接,访问多个web资源,关闭浏览器,这个过程可以称之为会话

**有状态会话:** 一个同学来过教室,他下次再来的时候,我们会知道这个同学,曾经来过,称之为有状态会话

一个网站,怎么证明你来过?

客户端  服务端

	1. 服务端给客户端一个信件,客户端下次访问服务端带上信件就行了; **(Cookie)**
 	2. 服务端登记你来过了,下次你来的时候我匹配你; **(session)**

### 7.2、保存会话的两种技术

**Cookie**

+ 客户端技术  (响应、请求)

**Session**

+ 服务器技术，利用这个技术，可以保存用户的会话信息？我们可以吧信息或者数据放在Seesion中

常见场景： 网站登录之后，你下次不用再登录了，第二次就直接访问进去了

### 7.3、Cookie

![image-20200703162855828](javaweb.assets\image-20200703162855828.png)

​																									     **图例**

1. 从请求中拿到Cookie信息
2. 服务器响应给客户端Cookie

```java
Cookie[] cookies = req.getCookies(); //获取Cookie
cookie.getName(); // 获取cookie中的key
cookie.getValue(); // 获取cookie中的value
new Cookie("LocalDateTime", System.currentTimeMillis() + ""); //创建一个cookie
cookie.setMaxAge(24*60*60); //设置cookie的有效期
// 因为cookie的key的不可重复性,所以可以通过重写一个key值一样的Cookie,并且将cookie的有效期设置为0来达到清除cookie的效果
```



一个网站cookie是否存在上限

+ 一个cookie只能保存一个信息;
+ 一个web站点可以给浏览器发多个cookie,最多存放20个cookie
+ cookie大小上限是4kb
+ 300个cookie是一个浏览器的上限 

删除cookie

+ 不设置有效期,关闭浏览器,zidongshixiao
+ 设置有效期为0;



编码解码

```java
URLEncoder.encode("陶云鹏","utf-8");

URLDecoder.decode(cookie.getValue(), "utf-8");
```



### 7.4、Session(重点)

![image-20200703163314029](javaweb.assets\image-20200703163314029.png)

​																											**图例**

什么是Session:

+ 服务器会给每个用户(浏览器)创建一个Session对象;
+ 一个Session独占一个浏览器,只要浏览器没有关闭,这个Session就存在;
+ 用户登录后,整个网站他都可以访问



```java
// 设置一个Session
public class SessionDemo01 extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // 解决中文乱码问题
        req.setCharacterEncoding("utf-8");
        resp.setCharacterEncoding("utf-8");
        resp.setContentType("text/html;charset=utf-8");

        // 获取Session
        HttpSession session = req.getSession();
        //给Session中存东西
        session.setAttribute("name","陶云鹏");
        // 获取Session的id
        String id = session.getId();
        // 判断Session是否是新创建
        if (session.isNew()){
            resp.getWriter().write("Session创建成功.ID: " + id);
        } else {
            resp.getWriter().write("Session已经在服务器中存在了.ID: " + id);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }

}
```

```java
// 获取Session中节点的内容
public class SessionDemo02 extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // 解决中文乱码问题
        req.setCharacterEncoding("utf-8");
        resp.setCharacterEncoding("utf-8");
        resp.setContentType("text/html;charset=utf-8");

        // 获取Session
        HttpSession session = req.getSession();
        //从Session中取东西
        Object name = session.getAttribute("name");
        if(name != null) {
            resp.getWriter().write((String) name);
        } else {
            resp.getWriter().write("您还没有Session信息");
        }

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
```

```java
// 手动清除session
public class SessionDemo03 extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // 解决中文乱码问题
        req.setCharacterEncoding("utf-8");
        resp.setCharacterEncoding("utf-8");
        resp.setContentType("text/html;charset=utf-8");

        // 获取Session
        HttpSession session = req.getSession();

        session.removeAttribute("name");
        session.invalidate();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
```



> **除了上面代码3中的手动清除session的方式外,我们也可以通过配置设定session的过期时间来达到清除session的目的**

```xml
<!--session的一些设置,可以设置cookie,session的过期时间,以及表示使用什么技术保持和跟踪会话-->
<session-config>  
    <!--session的失效时间,以分钟为单位-->
    <session-timeout>30</session-timeout>  
    <!--cookie的设置-->
    <cookie-config>  
        <!--设置SessionId在Cookie中的名称-->
        <name>JSESSIONID</name>
        <!--子域，指定在该子域下才可以访问Cookie，例如要让Cookie在a.test.com下可以访问，但在b.test.com下不能访问，则可将domain设置成a.test.com。-->
        <domain>ryesun.com</domain> 
        <!--设置SessionId存在哪个路径下，跟路径则可全站使用-->
        <path>/ryesun</path> 
        <!--该Cookie的用处说明。浏览器显示Cookie信息的时候显示该说明-->
        <comment>....</comment>  
        <!--设置是否只读-->
        <http-only>true</http-only> 
        <!--设置安全机制，只有https才能获取-->
        <secure>true</secure> 
        <!--设置SessionId30分钟后过期-->
        <max-age>1800</max-age>  
    </cookie-config>  
    <tracking-mode>COOKIE</tracking-mode>  
    <tracking-mode>URL</tracking-mode>  
    <tracking-mode>SSL</tracking-mode>  
</session-config>
```

> tracking-mode用于表示使用什么技术保持和跟踪会话
>
> 共有如下选择：
>
> - URL  容器将只在URL中内嵌会话ID，非常不安全
>
> - COOKIE 容器将使用会话cookie追踪会话ID，非常安全
>
> - SSL  容器将使用SSL会话ID作为HTTP会话ID，最安全的方式，但要求所有请求都必须是HTTPS
>
>   
>
> tracking-mode可以配置多个值，如上述示例，表示使用多种策略



**Session和Cookie的区别**

> 区别：
> 1、数据存放位置不同：
>
> cookie数据存放在客户的浏览器上，session数据放在服务器上。
> 2、安全程度不同：
>
> cookie不是很安全，别人可以分析存放在本地的COOKIE并进行COOKIE欺骗,考虑到安全应当使用session。
> 3、性能使用程度不同：
>
> session会在一定时间内保存在服务器上。当访问增多，会比较占用你服务器的性能,考虑到减轻服务器性能方面，应当使用cookie。
>
> 4、数据存储大小不同：
>
> 单个cookie保存的数据不能超过4K，很多浏览器都限制一个站点最多保存20个cookie，而session则存储与服务端，浏览器对其没有限制。
>
> 5、会话机制不同
>
> session会话机制：session会话机制是一种服务器端机制，它使用类似于哈希表（可能还有哈希表）的结构来保存信息。
>
> cookies会话机制：cookie是服务器存储在本地计算机上的小块文本，并随每个请求发送到同一服务器。 Web服务器使用HTTP标头将cookie发送到客户端。在客户端终端，浏览器解析cookie并将其保存为本地文件，该文件自动将来自同一服务器的任何请求绑定到这些cookie。

## 8 JSP

### 8.1、什么是jsp

Java Server Pages: Java服务器端页面,和Servlet一样,用于动态Web技术!

最大的特点: 

+ 写JSP就像在写HTML

区别: 

+ HTML只给用户提供静态的数据
+ JSP页面可以嵌入JAVA代码,为用户提供动态数据;



### 8.2、JSP原理

思路: JSP到底怎么运行的?

**浏览器向服务器发送请求,不管访问什么资源,其实都是在访问Servlet**

JSP最终也会被转化为一个java类

**JSP本质上就是一个Servlet**

```java
  // 初始化
  public void _jspInit() {
  }
  // 销毁
  public void _jspDestroy() {
  }
  // jsp service
  public void _jspService(HttpServletRequest request, HttpServletResponse response)
```

1. 判断请求

```java
if (!"GET".equals(_jspx_method) && !"POST".equals(_jspx_method) && !"HEAD".equals(_jspx_method) && !javax.servlet.DispatcherType.ERROR.equals(request.getDispatcherType())) {
      response.sendError(HttpServletResponse.SC_METHOD_NOT_ALLOWED, "JSPs only permit GET POST or HEAD");
      return;
    }
```

2. 内置对象

```java
final javax.servlet.jsp.PageContext pageContext;    //页面上下文
javax.servlet.http.HttpSession session = null;      //session
final javax.servlet.ServletContext application;     //applicationContext
final javax.servlet.ServletConfig config;           //config,配置
javax.servlet.jsp.JspWriter out = null;             //out,输出
final java.lang.Object page = this;                 //page,当前页
HttpServletRequest request                          //请求
HttpServletResponse response                        //响应
```

3. 输入页面前增加的代码

```java
response.setContentType("text/html");   // 设置响应的页面类型
pageContext = _jspxFactory.getPageContext(this, request, response, null, true, 8192, true);
_jspx_page_context = pageContext;  //
application = pageContext.getServletContext();
config = pageContext.getServletConfig();
session = pageContext.getSession();
out = pageContext.getOut();
_jspx_out = out;
```

4.以上这些我们可以在jsp中直接使用

![image-20200706164056434](javaweb.assets\image-20200706164056434.png)

在JSP页面中

只要是java代码就会被原封不动的输出

如果是HTML代码,就会被转换为 out.write();



### 8.3、JSP基础语言

任何语言都有自己的语法,JAVA中有,jsp作为java技术的一种应用,它除了有java的所有语法外,还拥有一些自己扩充的语法.



#### **JSP表达式**

```jsp
<%--JSP表达式的作用: 用来将程序的输入,输出到客户端--%>
    <%=
      new java.util.Date()
    %>
```

#### **JSP脚本片段**

```jsp
<%--jsp脚本片段--%>
  <%
    int sum = 0;
    for (int i = 0; i < 100; i++) {
      sum += i;
    }
    out.println(sum);
  %>
```

**脚本片段的再实现**

```jsp
<%
	int x = 10;
	out.println(x);
%>
<p>这是一个JSP文档</p>
<%
	int y = 2;
	out.println(y);
%>

<hr>

<%--在代码嵌入HTML元素--%>
<%
	for (int i = 0; i < 5; i++){
%>
<h1>
    hello,world <%=i%>
</h1>
<%
	}
%>
```

#### **JSP声明**

```jsp
<%!
	static {
    	System.out.println("Loding Servlet!");
    }
	
	private int globalvar = 0;
	
	public void show(){
       System.out.println("正在展示!");
    }
    
%>
```



JSP声明,会被编译到JSP生成的java的类中! 其他的, 就会被生成到_jspService方法中!

> jsp的注解,不会再客户端显示,HTML就会



### 8.4、JSP指令

>在页面上的配置

```jsp
<%@ page args...  %> 
  		1.import 导入资源
  		2.errorPage  错误自动跳转页面
		3.isErrorPage 显示的生命这是个错误页面
<%@include args...%>  //包含
<%@taglib args ...%>
```

> 在xml上的配置

```xml
<error-page>
	<error-code>404</error-code>
    <location>/error/404.jsp</location>
</error-page>
<error-page>
	<error-code>500</error-code>
    <location>/error/500.jsp</location>
</error-page>
```



### 8.5、JSP九大内置对象

+ PageContext
+ Request
+ Response
+ Session
+ Application ( ServletContext )
+ Config ( SercletConfig )
+ out
+ page
+ Exception

| 对象名      | 作用域                                                |
| ----------- | ----------------------------------------------------- |
| pageContext | 保存的数据只在一个页面有效                            |
| request     | 保存的数据只在一次请求中有效,请求转发会携带这个数据   |
| session     | 保存的数据只在一次会话中有效,从打开浏览器到关闭浏览器 |
| application | 保存的数据只在服务器中有效,从打开服务器到关闭服务器   |

