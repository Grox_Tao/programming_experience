## **本文档是在观看 *江南一点雨* 大大的Spring Security加以修改搭配上自己的理解,实践整合完成的,看Spring Security完整版请转到http://www.itboyhub.com/springsecurity/index.html观看,如有侵权,告知立删.**





# Spring Security

## 1. Spring Security介绍

**Spring  Security是一个能够为基于Spring的企业应用系统提供声明式的安全访问控制解决方案的安全框架。**

**它提供了一组可以在Spring应用上下文中配置的Bean，充分利用了Spring IoC，DI（控制反转Inversion of Control  ,DI:Dependency Injection  依赖注入）和AOP（面向切面编程）功能，为应用系统提供声明式的安全访问控制功能，减少了为企业系统安全控制编写大量重复代码的工作。**



在Spring  Security之前其实也有一个安全框架,叫做Shiro , Shiro 有着众多的优点，例如轻量、简单、易于集成等。

当然 Shiro 也有不足，例如对 OAuth2 支持不够，在 Spring Boot 面前无法充分展示自己的优势等等，特别是随着现在 Spring Boot 和 Spring Cloud 的流行，Spring Security 正在走向舞台舞台中央。

