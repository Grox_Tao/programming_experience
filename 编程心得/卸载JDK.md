**卸载JDK** 

1.  在环境变量中的JAVA_HOME中找到Java的路径,删除整个文件夹
2.  删除环境变量中的JAVA_HOME
3.  删除path中关于Java的目录
4.  在cmd中输入java -version

**安装JDK**

1. 下载jdk/根据电脑版本

2. 双击安装

3. 修改安装盘符(记住路径)

4. 配置环境变量

   此电脑--->属性--->高级设置--->环境变量

   创建JAVA_HOME 将jdk路径放入

   配置path变量(%JAVA_HOME%/bin)

5. 测试JDK是否成功   cmd --->java -version

