/**
 * Title: excel.java
 * @author Grox
 * @date 2020年1月9日 下午3:37:59
 * @version 1.0
 */
package com.sgcc.sgga.daily.excel.model;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ExcelBaseParams;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import cn.afterturn.easypoi.excel.entity.enmus.ExcelType;

/**
 * @author Grox
 * @method excel.java
 * @CNmethod 
 */

public class EasyPoiUtils {

    /**
     *  导出excel
     * @param pojoClass
     * @param dataSet
     * @param path
     * @param filename
     * @throws IOException
     */
    public static void exportExcel(Class<?> pojoClass, Collection<?> dataSet,String path,String filename) throws IOException {

        File savefile = new File(path);
        if (!savefile.exists()) {
            savefile.mkdirs();
        }
        Workbook workbook = ExcelExportUtil.exportExcel(new ExportParams(), pojoClass, dataSet);
        FileOutputStream fos = new FileOutputStream(path+filename);
        workbook.write(fos);
        fos.close();
    }

    /**
     * 根据Map创建对应的Excel(一个excel 创建多个sheet)
     * @param list list 多个Map key title 对应表格Title key entity 对应表格对应实体 key data
     *      *             Collection 数据
     * @param path 路径
     * @param filename&emsp;文件名
     * @throws IOException
     */
    public static void  exportExcel(List<Map<String, Object>> list,String path,String filename) throws IOException{
        File savefile = new File(path);
        if (!savefile.exists()) {
            savefile.mkdirs();
        }
        Workbook workbook = ExcelExportUtil.exportExcel(list, ExcelType.HSSF);

        FileOutputStream fos = new FileOutputStream(path+filename);
        workbook.write(fos);
        fos.close();
    }


    /**
     * 导入excel
     * @param file
     * @param pojoClass
     * @param params
     * @param <T>
     * @return
     */
    public static <T>List<T> importExcel(File file, Class<?> pojoClass, ImportParams params){
        long start = new Date().getTime();
        List<T> list = ExcelImportUtil.importExcel(file,UserEntity.class, params);
        return list;
    }
    
    /**
     * 导出流文件表
     * @author Grox
     * @param exportParams
     * @param pojoClass
     * @param dataSet
     * @param response
     * @throws IOException
     */
    public static void exportExcel(ExportParams exportParams,Class<?> pojoClass, Collection<?> dataSet,HttpServletResponse response) throws IOException {
    	// 以下文件头设置,都为.xls
    	response.setContentType("application/vnd.ms-excel");
    	// sitevehicleinformation.xls是弹出下载对话框的文件名，不能为中文，中文请自行编码
    	response.setHeader("Content-Disposition", "attachment;filename=excel.xls");
        Workbook workbook = ExcelExportUtil.exportExcel(exportParams, pojoClass, dataSet);
        ServletOutputStream o = response.getOutputStream();
        workbook.write(o);
        o.close();
    }

	

	
}

