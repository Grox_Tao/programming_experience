/**
 * Title: ghfh.java
 * @author Grox
 * @date 2020年1月10日 上午9:43:13
 * @version 1.0
 */
package com.sgcc.sgga.daily.excel.model;

import java.util.List;

import cn.afterturn.easypoi.excel.annotation.Excel;
import cn.afterturn.easypoi.excel.annotation.ExcelTarget;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Grox
 * @method ghfh.java
 * @CNmethod 
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ExcelTarget("teacherEntity")
public class TeacherEntity implements java.io.Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String id;
    /** name */
    @Excel(name = "主讲老师_major,代课老师_absent", orderNum = "1", isImportField = "true_major,true_absent" ,needMerge = true)
    private String name;
}