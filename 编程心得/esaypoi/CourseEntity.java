/**
 * Title: fdghgfd.java
 * @author Grox
 * @date 2020年1月10日 上午9:41:18
 * @version 1.0
 */
package com.sgcc.sgga.daily.excel.model;

import java.util.List;

import cn.afterturn.easypoi.excel.annotation.Excel;
import cn.afterturn.easypoi.excel.annotation.ExcelCollection;
import cn.afterturn.easypoi.excel.annotation.ExcelEntity;
import cn.afterturn.easypoi.excel.annotation.ExcelTarget;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Grox
 * @method fdghgfd.java
 * @CNmethod 
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ExcelTarget("courseEntity")
public class CourseEntity implements java.io.Serializable {
   /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
/** 主键 */
   private String        id;
   /** 课程名称 */
   @Excel(name = "课程名称", orderNum = "1", width = 25,needMerge = true)
   private String        name;
   /** 老师主键 */
   @ExcelEntity(id = "absent")
   private TeacherEntity mathTeacher;

   @ExcelCollection(name = "学生", orderNum = "4")
   private List<StudentEntity> students;
}
