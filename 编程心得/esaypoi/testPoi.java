/**
 * Title: dsada.java
 *
 * @author Grox
 * @date 2020年1月9日 下午4:11:27
 * @version 1.0
 */
package com.sgcc.sgga.daily.excel.model;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import com.sgcc.sgga.statistics.model.CountInfo;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ylkj.cloud.core.response.Result;
import com.ylkj.cloud.core.response.ResultGenerator;

import cn.afterturn.easypoi.excel.entity.ExportParams;

/**
 * @author Grox
 * @method testPoi.java
 * @CNmethod
 */
@RestController
@RequestMapping("/testPoi")
@CrossOrigin
public class testPoi {
    /**
     * 测试单sheet导出
     *
     * @throws IOException
     */
    @GetMapping("/excel")
    public Result testExportExcel(HttpServletResponse response) throws IOException {
        List<StudentEntity> list = new ArrayList<>();
        List<CourseEntity> list1 = new ArrayList<>();
        StudentEntity s = new StudentEntity();
        CourseEntity c = new CourseEntity();
        s.setId("1");
        s.setName("陶云鹏");
        s.setSex(1);
        s.setBirthday(new Date());
        s.setRegistrationDate(new Date());
        list.add(s);
        list.add(new StudentEntity("2","郭鑫",2, new Date(),new Date()));
        c.setId("1");
        c.setName("语文");
        c.setMathTeacher(new TeacherEntity("1","老安"));
        c.setStudents(list);
        list1.add(c);
        EasyPoiUtils.exportExcel(new ExportParams("计算机一班学生","学生1"), CourseEntity.class,list1, response);
        return ResultGenerator.genOkResult("导出成功");
    }

}
