package com.sgcc.sgga.tools;

/*
 * @ClassName ExcelNull
 * @CNname
 * @Author Grox
 * @Date 2020/6/23 9:40 周二
 * @Version 1.0
 */

import com.ylkj.cloud.core.response.Result;
import com.ylkj.cloud.core.response.ResultGenerator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ExcelNull {
    private ExcelNull() {

    }

    public static Result excelNull(HttpServletRequest request, HttpServletResponse response) {
        try {
            request.getRequestDispatcher("/nullexcel/excel").forward(request, response);
        } catch (ServletException | IOException e) {
            e.printStackTrace();
        }
        return ResultGenerator.genOkResult("导出成功,数据为空");
    }
}
