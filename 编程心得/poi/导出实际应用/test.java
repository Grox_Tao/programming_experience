
    @GetMapping("/excel")
    public Result excel(HttpServletRequest request, HttpServletResponse response) {
     
            List<Map<String, String>> excelList = new ArrayList<>();
            // 获取表中全部数据 list的?类型根据导出的实体类来填写
            List<?> list = Service.getAll();

            // 空判,如果查询出来的数据集合是空的,则导出空表
            if (ObjectUtil.isEmpty(list)) {
                return ExcelNull.excelNull(request, response);
            }
            excelList = this.dataProcessing(list);
            // 调用文件导出工具类
            return ExcelTools.excel(request, response, excelList, ExcelTools.getMapSize(excelList), CONTENT, excelList.size());
        } catch (Exception e) {
            e.printStackTrace();
            return ResultGenerator.genFailedResult("导出失败");
        }
    }

    /**
         * 选中导出
         *
         * @param request
         * @param response
         * @return
         */
        @GetMapping("/excel/{ids}")
        public Result excel(HttpServletRequest request, HttpServletResponse response, @PathVariable("ids") String ids) {
            // 将传来的id串根据逗号分割
            String[] split = ids.split(",");
            List<BsApmAirportAreaTour> datas = new LinkedList<>();
            for (String id : split) {
                // 根据id查询 ?类型根据导出的实体类来填写
                ? result = Service.selectOnlyOne(Integer.valueOf(id));
                // 将查询出来的数据转换一下类型
                ? data = (?) InputInjectFilter.encodeInputString(result, ?.class);
                datas.add(data);
            }
            // 空判,如果查询出来的数据集合是空的,则导出空表
            if (ObjectUtil.isEmpty(datas)) {
                return ExcelNull.excelNull(request,response);
            }
            // 调用数据处理方法,
            List<Map<String, String>> excelList = dataProcessing(datas);
            // 调用文件导出工具类
            try {
                return ExcelTools.excel(request, response, excelList, ExcelTools.getMapSize(excelList), CONTENT ,excelList.size());
            } catch (IOException e) {
                e.printStackTrace();
                return ResultGenerator.genFailedResult("导出失败");
            }
        }

    /**
     * 数据处理
     *
     * @return 返回要导出的数据集合
     */
    public List<Map<String, String>> dataProcessing(List<?> list) {
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        List<Map<String, String>> excelList = new ArrayList<>();
        // 获取表中全部数据
        for (? b : list) {
            Map<String, String> map = new LinkedHashMap<>();
			// 数据处理实例
			// map的key是字段名, value是具体的值
            map.put("天气", b.getWeather());
            map.put("道面积雪", b.getSnowsTypeName());
            map.put("异常情况处置", b.getAbnormalSituationManagement());
            excelList.add(map);
        }
        return excelList;
    }