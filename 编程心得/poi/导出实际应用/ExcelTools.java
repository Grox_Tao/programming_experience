package com.sgcc.sgga.tools;

/*
 * @ClassName ExcelTools
 * @CNname
 * @Author Grox
 * @Date 2020/6/23 9:39 周二
 * @Version 1.0
 */

import cn.hutool.core.io.IoUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.poi.excel.ExcelUtil;
import cn.hutool.poi.excel.ExcelWriter;
import com.ylkj.cloud.core.response.Result;
import com.ylkj.cloud.core.response.ResultGenerator;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import java.net.URLEncoder;
import java.util.List;
import java.util.Map;

public class ExcelTools {
    private ExcelTools() {
    }

    public static Result excel(HttpServletRequest request, HttpServletResponse response,
                               List<Map<String, String>> excelList, Integer l, String title, Integer listSize) throws IOException {
        ExcelWriter writer = ExcelUtil.getWriter();
        ServletOutputStream out = null;
        try {
            // 数据为空时,导出空表
            if (ObjectUtil.isEmpty(excelList)) {
                return ExcelNull.excelNull(request, response);
            }
            // 合并表头
            writer.merge(l - 1, title);
            // 导出表
            writer.write(excelList, true);
            //设置列宽/行高
//            writer.setColumnWidth(-1, 20);
//			for(int i = 0; i <= listSize + 1; i++){
//				writer.setRowHeight(i,20);
//			}
            // 以下文件头设置,都为.xls
            response.setContentType("application/vnd.ms-excel");
            // sitevehicleinformation.xls是弹出下载对话框的文件名，不能为中文，中文请自行编码
            response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(title,"utf-8") +  ".xls");
            out = response.getOutputStream();
            writer.flush(out, true);
            return ResultGenerator.genOkResult("导出成功");
        }finally {
            // 关闭writer，释放内存
            writer.close();
            // 关闭输出Servlet流
            IoUtil.close(out);
        }
    }


    /**
         * 表头合并的计算
         * @param excelList
         * @return
         */
        public static Integer getMapSize(List<Map<String, String>> excelList) {
            if (ObjectUtil.isNotEmpty(excelList)) {
                return excelList.get(0).size();
            } else {
                return 2;
            }
        }
}
