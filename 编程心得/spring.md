# spring

## 1. spring

### 1. 简介

+ Spring: 春天------>给软件行业带来了春天.
+ 2002年,首次推出了Spring的雏形: interface21!
+ Spring框架是以interface21框架为基础,经过重新设计,并不断丰富其内涵,与2004年3月24日,发布了1.0版本

+ **Rod Johnson**, Spring Framework创始人，著名作者。 Rod在[悉尼大学](https://baike.baidu.com/item/悉尼大学/1700805)不仅获得了计算机学位，同时还获得了音乐学位。更令人吃惊的是在回到软件开发领域之前，他还获得了音乐学的博士学位。 
+ spring理念: 使现有的技术更加容易使用,本身是一个大杂烩,整合了现有的技术框架.
+ spring目的：解决企业应用开发的复杂性



+ SSH : struts2 + spring + hibernate

+ SSM: springmvc + spring + mybatis!

官网: https://spring.io/projects/spring-framework#overview

官方下载地址: https://repo.spring.io/release/org/springframework/spring

GitHub: https://github.com/spring-projects/spring-framework



```xml
<!-- https://mvnrepository.com/artifact/org.springframework/spring-webmvc -->
<dependency>
    <groupId>org.springframework</groupId>
    <artifactId>spring-webmvc</artifactId>
    <version>5.2.8.RELEASE</version>
</dependency>
<!-- https://mvnrepository.com/artifact/org.springframework/spring-webmvc -->
<dependency>
    <groupId>org.springframework</groupId>
    <artifactId>spring-jdbc</artifactId>
    <version>5.2.8.RELEASE</version>
</dependency>

```



### 2. 优点

+ Spring是一个开源的免费的框架(容器)
+ Spring是一个轻量级,非入侵式的框架
+ 控制反转(IOC),依赖注入(DL),面向切面编程(AOP)
+ 支持事务的处理,对框架整合的支持

==总结一句话: Spring是一个轻量级的控制反转和面向切面编程的框架==



### 3. 组成

![image-20200731095737473](spring.assets/image-20200731095737473.png)	



### 4. 拓展



![image-20200731100943269](spring.assets/image-20200731100943269.png)



+ Spring Boot
  + 一个快速开发的脚手架.
  + 基于Spring Boot可以快速的开发单个微服务
  + 约定大于配置
+ Spring Cloud
  + Spring Cloud是基于Spring Boot实现的



因为现在大多数公司都在使用Spring Boot进行快速开发,学习Spring Boot的前提,需要完全掌握Spring及Spring MVC! 起到了承上启下的作用!  

**弊端: 发展了太久之后, 违背了原来的理念! 配置十分繁琐,人称"配置地狱"**



## 2. IOC推理导论



IOC是Inversion of Control的缩写，多数书籍翻译成“控制反转”。

1996年，Michael Mattson在一篇有关探讨面向对象框架的文章中，首先提出了IOC 这个概念。对于面向对象设计及编程的基本思想，前面我们已经讲了很多了，不再赘述，简单来说就是把复杂系统分解成相互合作的对象，这些对象类通过封装以后，内部实现对外部是透明的，从而降低了解决问题的复杂度，而且可以灵活地被重用和扩展。



IOC理论提出的观点大体是这样的：借助于“第三方”实现具有依赖关系的对象之间的解耦。如下图：
![在这里插入图片描述](spring.assets/20190403225122217.png)
大家看到了吧，由于引进了中间位置的“第三方”，也就是IOC容器，使得A、B、C、D这4个对象没有了耦合关系，齿轮之间的传动全部依靠“第三方”了，全部对象的控制权全部上缴给“第三方”IOC容器，所以，IOC容器成了整个系统的关键核心，它起到了一种类似“粘合剂”的作用，把系统中的所有对象粘合在一起发挥作用，如果没有这个“粘合剂”，对象与对象之间会彼此失去联系，这就是有人把IOC容器比喻成“粘合剂”的由来。



我们再来做个试验：把上图中间的IOC容器拿掉，然后再来看看这套系统：
![在这里插入图片描述](spring.assets/20190403225133997.png)
我们现在看到的画面，就是我们要实现整个系统所需要完成的全部内容。这时候，A、B、C、D这4个对象之间已经没有了耦合关系，彼此毫无联系，这样的话，当你在实现A的时候，根本无须再去考虑B、C和D了，对象之间的依赖关系已经降低到了最低程度。所以，如果真能实现IOC容器，对于系统开发而言，这将是一件多么美好的事情，参与开发的每一成员只要实现自己的类就可以了，跟别人没有任何关系！

我们再来看看，控制反转(IOC)到底为什么要起这么个名字？我们来对比一下：

软件系统在没有引入IOC容器之前，如图1所示，对象A依赖于对象B，那么对象A在初始化或者运行到某一点的时候，自己必须主动去创建对象B或者使用已经创建的对象B。无论是创建还是使用对象B，控制权都在自己手上。

软件系统在引入IOC容器之后，这种情形就完全改变了，如图3所示，由于IOC容器的加入，对象A与对象B之间失去了直接联系，所以，当对象A运行到需要对象B的时候，IOC容器会主动创建一个对象B注入到对象A需要的地方。

通过前后的对比，我们不难看出来：对象A获得依赖对象B的过程,由主动行为变为了被动行为，控制权颠倒过来了，这就是“控制反转”这个名称的由来.



**学习IOC前后的开发对比**

![image-20200731114404681](spring.assets/image-20200731114404681.png)



## 3. IOC本质

IOC是一种设计实现思想, DL是实现IOC的一种方法,也有人认为DL只是IOC的另一种说法.没有IOC的程序中,我们使用面向对象的编程,对象的创建与对象间的依赖关系完全硬编码在程序中,对象的创建由程序自己控制,控制反转后将对象的创建转移给第三方.所谓控制反转就是: 获取依赖对象的方式反转了.



**spring工作视图**

![image-20200731130335872](spring.assets/image-20200731130335872.png)

## 4. HelloSpring

```java
// 创建实体类
public class hello {
    private String str;

    public String getStr() {
        return str;
    }

    public void setStr(String str) {
        this.str = str;
    }

    @Override
    public String toString() {
        return "hello{" +
                "str='" + str + '\'' +
                '}';
    }
}
```

```xml
<!--创建ioc核心配置-->
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://www.springframework.org/schema/beans
                      http://www.springframework.org/schema/beans/spring-beans-4.0.xsd">
    <!--使用Spring来创建对象,在Spring中统称为Bean-->
    <bean id="hello" class="com.grox.model.hello">
        <property name="str" value="spring"/>
    </bean>


</beans>
```

```java
public class MyTest {
    public static void main(String[] args) {
        // 获取Spring的上下文对象
        ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
        // 我们的对象现在都在Spring中被管理了,我们要使用,直接去里面取出来就可以!
        hello hello = (hello) context.getBean("hello");
        System.out.println(hello.toString());
    }
}
```

## 5. IOC创建对象的方式

1. 使用无参构造创建对象,默认
2. 假设我们要使用有参构造创造对象

```xml
<!--下标赋值,索引是实体类的顺序,从0开始-->
<bean id="exampleBean" class="examples.ExampleBean">
    <constructor-arg index="0" value="7500000"/>
    <constructor-arg index="1" value="42"/>
</bean>
```

```xml
<!--通过类型创建,但是不建议使用,一旦出现重复字段类型,就会出错-->
<bean id="exampleBean" class="examples.ExampleBean">
    <constructor-arg type="int" value="7500000"/>
    <constructor-arg type="java.lang.String" value="42"/>
</bean>
```

```xml
<!--通过参数名创建,-->
<beans>
    <bean id="beanOne" class="x.y.ThingOne">
        <constructor-arg name="beanTwo" value="beanto"/>
    </bean>
</beans>
```

**总结: 在配置文件加载的时候,容器中管理的对象就已经初始化了**



## 6. Spring配置

### 1. 别名

```xml
<alias name="hello" alias="11"/>
```



### 2. Bean的配置

```xml
<!--
        id: bean的唯一标识符,也就是相当于我们的对象名
        class: bean对象所对应的权限定名 包名 + 类型
        name: 别名 ,而且可以同时取多个别名
    -->
    <bean id="hello1" class="com.grox.model.hello" name="hello2">
        <property name="str" value="spring"/>
    </bean>
```



### 3. import

import一般用于团队开发使用,他可以将多个配置文件,导入合并为一个

假设,现在项目中有多个人开发,这三个复制不同的类开发,不同的类需要注册在不同的bean中,我们可以利用import将所有人的beans.xml合并在一起

```xml
<import resource="beans.xml"/>
```

使用的时候,直接使用总的配置就可以了



## 7. 依赖注入

### 1. 构造器注入

```java
// 创建实体类
public class hello {
    private String str;

    public String getStr() {
        return str;
    }

    public void setStr(String str) {
        this.str = str;
    }

    @Override
    public String toString() {
        return "hello{" +
                "str='" + str + '\'' +
                '}';
    }
}
```

```xml
<!--创建ioc核心配置-->
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://www.springframework.org/schema/beans
                      http://www.springframework.org/schema/beans/spring-beans-4.0.xsd">
    <!--使用Spring来创建对象,在Spring中统称为Bean-->
    <bean id="hello" class="com.grox.model.hello">
        <property name="str" value="spring"/>
    </bean>


</beans>
```

```java
public class MyTest {
    public static void main(String[] args) {
        // 获取Spring的上下文对象
        ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
        // 我们的对象现在都在Spring中被管理了,我们要使用,直接去里面取出来就可以!
        hello hello = (hello) context.getBean("hello");
        System.out.println(hello.toString());
    }
}
```



### 2. Set方法注入

```java
// Address 实体类
public class Address {
    private String address;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "Address{" +
                "address='" + address + '\'' +
                '}';
    }
}

```

```java
// Student 实体类
public class Student {
    private String name;
    private Address address;
    private String[] books;
    private List<String> hobbys;
    private Map<String,String> card;
    private Set<String> games;
    private String wife;
    private Properties info;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String[] getBooks() {
        return books;
    }

    public void setBooks(String[] books) {
        this.books = books;
    }

    public List<String> getHobbys() {
        return hobbys;
    }

    public void setHobbys(List<String> hobbys) {
        this.hobbys = hobbys;
    }

    public Map<String, String> getCard() {
        return card;
    }

    public void setCard(Map<String, String> card) {
        this.card = card;
    }

    public Set<String> getGames() {
        return games;
    }

    public void setGames(Set<String> games) {
        this.games = games;
    }

    public String getWife() {
        return wife;
    }

    public void setWife(String wife) {
        this.wife = wife;
    }

    public Properties getInfo() {
        return info;
    }

    public void setInfo(Properties info) {
        this.info = info;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", address=" + address.toString() +
                ", books=" + Arrays.toString(books) +
                ", hobbys=" + hobbys +
                ", card=" + card +
                ", games=" + games +
                ", wife='" + wife + '\'' +
                ", info=" + info +
                '}';
    }
}
```

```xml
<!-application-configuration.xml-->
 <bean id="address" class="com.grox.model.Address">
        <property name="address" value="北京"/>
    </bean>

    <bean id="student" class="com.grox.model.Student">
        <!--普通值注入-->
        <property name="name" value="陶云鹏"/>
        <!--bean注入-->
        <property name="address" ref="address"/>
        <!--数组-->
        <property name="books">
            <array>
                <value>福尔摩斯</value>
                <value>柯南</value>
                <value>狄仁杰</value>
            </array>
        </property>
        <!--list集合-->
        <property name="hobbys">
            <list>
                <value>唱</value>
                <value>跳</value>
                <value>篮球</value>
            </list>
        </property>
        <!--map集合-->
        <property name="card">
            <map>
                <entry key="身份证" value="121212"/>
                <entry key="健康证" value="123214"/>
            </map>
        </property>
        <!--set集合-->
        <property name="games">
            <set>
                <value>lol</value>
                <value>data</value>
                <value>sekiro</value>
            </set>
        </property>
        <!--null-->
        <property name="wife">
            <null/>
        </property>
        <!--Properties-->
        <property name="info">
            <props>
                <prop key="java">java</prop>
            </props>
        </property>
    </bean>
```

+ 测试

```java
public class MyTest {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
        Student student = (Student) context.getBean("student");
        System.out.println("student = " + student);
    }
}
```



### 3. 拓展方式

####  1. p命名空间注入

需要在bean.xml中的头部规范中添加p标签的规范

```xml
xmlns:p="http://www.springframework.org/schema/p"
```

```xml
<!--p命名空间注入, 可以直接注入属性的值: property-->
    <bean id="user" class="com.grox.model.User" p:name="陶云鹏" p:address="北京"/>
```



#### 2. c命名空间注入

```xml
 xmlns:c="http://www.springframework.org/schema/c"
```

```xml
<!--c命名空间注入, 通过构造器注入: construct-args-->
    <bean id="user2" class="com.grox.model.User" c:name="Grox" c:address="beijing"/>
```



### 4. bean的作用域

![image-20200811095932624](spring.assets/image-20200811095932624.png)



![image-20200811100137430](spring.assets/image-20200811100137430.png)



#### 1.  The Singleton Scope (单例模式)

![image-20200811100319984](spring.assets/image-20200811100319984.png)

只管理一个单例bean的共享实例，并且所有对具有一个或多个ID的bean的请求都与该bean定义相匹配，从而导致Spring容器返回一个特定的bean实例。换句话说，当您定义一个bean定义并且它的作用域是一个单例对象时，Spring IoC容器会创建该bean定义定义的对象的一个实例。这个单一实例存储在这样的单例bean的缓存中，对这个已命名bean的所有后续请求和引用都会返回缓存的对象。



#### 2. The Prototype Scope(原型模式)

![image-20200811100915168](spring.assets/image-20200811100915168.png)

当一个bean的作用域为Prototype，表示一个bean定义对应多个对象实例。Prototype作用域的bean会导致在每次对该bean请求（将其注入到另一个bean中，或者以程序的方式调用容器的getBean()方法）时都会创建一个新的bean实例。Prototype是原型类型，它在我们创建容器的时候并没有实例化，而是当我们获取bean的时候才会去创建一个对象，而且我们每次获取到的对象都不是同一个对象。根据经验，对有状态的bean应该使用prototype作用域，而对无状态的bean则应该使用singleton作用域。在XML中将bean定义成prototype.



#### 3. 其余的request、session、application等这些只能在web开发中用到。



## 8. Bean的自动装配

+ 自动装配是Spring满足bean依赖的一种方式!
+ Spring会在上下文中自动寻找,并自动给bean装配属性!



在Spring中有三种装配的方式

1. 在xml中显示的配置
2. 在java中显示配置
3. 隐式的自动装配bean[重要]



### 1. byName自动装配

```xml
<!--
        byName: 会自动在容器上下文中查找,和自己对象set方法后面的值对应的bean id!
		缺点: 因为是和set方法后面的值对应,所有在名字选定上有了限制,不能随便写
    -->
    <bean id="people" class="com.grox.model.People" autowire="byName">
        <property name="name" value="陶云鹏"/>
    </bean>
```

![image-20200811133311520](spring.assets/image-20200811133311520.png)



### 2. byType自动装配

```xml
<!--
        byType: 会自动在容器上下文中查找,和自己对象属性类型的bean!
		缺点: 如果有两个及以上相同类型的bean在配置中,容器会无法分辨导致直接报错
    -->
    <bean id="people" class="com.grox.model.People" autowire="byName">
        <property name="name" value="陶云鹏"/>
    </bean>
```



![image-20200811132532876](spring.assets/image-20200811132532876.png)

### 3. 使用注解自动装配

1. 导入约束: context约束
2. 配置注解支持: context:cnnotation-config

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
                           http://www.springframework.org/schema/beans/spring-beans-4.0.xsd
                           http://www.springframework.org/schema/context
                           http://www.springframework.org/schema/context/spring-context-4.0.xsd
                           http://www.springframework.org/schema/aop
                           http://www.springframework.org/schema/aop/spring-aop-4.0.xsd">
```



#### @Autowired

直接在属性上使用即可,也可以在set方式上使用

使用Autowired我们可以不用编写Set方法,前提是你这个自动装配的属性在IOC(Spring)容器中存在,且符合名字byname!

科普: 

```
@Nullable 字段标记了这个注解,说明这个字段可以为空
```

```java
public @interface Autowired {
    boolean required() default true;
}
```



![image-20200817113931845](spring.assets/image-20200817113931845.png)



![image-20200817114059526](spring.assets/image-20200817114059526.png)

#### @Qualifier

如果@Autowried自动装配环境比较复杂时，自动装配无法通过一个@Autowired注解完成时，我们可以使用@Qualifier(value = "xxx")注解配合@Autowired使用,来指定一个唯一的bean对象的注入.

==注意: bean的id绝对不可以相同==



#### @Resource

@Resource（这个注解属于J2EE的），默认按照名称进行装配，名称可以通过name属性进行指定，如果没有指定name属性，当注解写在字段上时，默认取字段名进行安装名称查找，如果注解写在setter方法上默认取属性名进行装配。当找不到与名称匹配的bean时才按照类型进行装配。但是需要注意的是，如果name属性一旦指定，就只会按照名称进行装配。

推荐使用：@Resource注解在字段上，这样就不用写setter方法了，并且这个注解是属于J2EE的，减少了与spring的耦合。这样代码看起就比较优雅。

## 9. 使用注解开发

在Spring4之后,要使用注解开发, 必须要保证aop的包导入

![image-20200817144448413](spring.assets/image-20200817144448413.png)

使用注解需要导入context约束,增加注解的支持

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
                           https://www.springframework.org/schema/beans/spring-beans-4.0.xsd
                           http://www.springframework.org/schema/context
                           https://www.springframework.org/schema/context/spring-context-4.0.xsd">
```

### 1. 注解开发中属性如何注入

```java
@Component
public class User {

    @Value("陶云鹏")
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
```

### 2. 衍生的注解

@Component有几个衍生注解,我们在web开发中,会按照mvc三层架构分层!

+ dao(@Repository)
+ service(@Service)
+ controller(@Controller)

这四个注解功能都是一样的,都是代表将某个类注册到Spring中,装配Bean!

**小结**

xml和注解

+ xml更加万能,适用于任何场合,维护简单方便
+ 注解不是直接类使用不了,维护相对复杂

xml与注解的最佳实践

+ xml用来管理bean
+ 注解只负责完成属性的注入
+ 我们在使用的过程中,只需要注意一个问题: 必须让注解生效,就需要开启注解的支持



```java
/** 
 * 实体类
 *
 */
@Component
public class User {
    @Value("陶云鹏")
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
```

```java
/** 
 * 配置类
 *
 */
// 这个也会被Spring容器托管,注册到容器中,因为他本来就是一个@Component
// @Configuration代表这是一个配置类,其效果等价于beans.xml
@ComponentScan("com.grox.model")
@Configuration
@import(配置类名字.class) // 在有多个配置类的情况下可以使用@import注解进行集成
public class GroxConfig {

    // 注册一个bean, 相当于我们之前写的bean标签
    // 这个方法的名字, 就相当于bean标签中的id属性
    // 这个方法的返回值,  就相当于bean便签中的class属性
    @Bean
    public User getUser() {
        return new User();  //这里就是返回要注入到bean的对象
    }
}
```

```java
/** 
 * 测试类
 *
 */
public class MyTest {
    public static void main(String[] args) {
       ApplicationContext context = new AnnotationConfigApplicationContext(GroxConfig.class);
        User user = (User) context.getBean("getUser");
        System.out.println(user.getName());
    }
}
```

## 10. 代理模式

### 1. 静态代理

静态代理模式 的好处

+ 可以使真实角色的操作更加纯粹, 不用去关注一些公共的业务
+ 公共的操作交给了代理角色,实现了业务的分工
+ 公共业务发生扩展的时候,方便集中管理

缺点

+ 一个真是的角色就会产生一个代理角色,代码量会翻倍

![image-20200819105531523](spring.assets/image-20200819105531523.png)

### 10.3 动态代理

+ 动态代理和静态代理角色一样
+ 动态代理的代理类是动态生成的,不是我们直接写好的
+ 动态代理分为两大类: 基于接口的动态代理和基于类的动态代理
  + 基于接口----- JDK动态代理
  + 基于类--- cglib
  + Java字节码实现: javassist



Proxy: 生成动态代理

InvocationHandler: 处理代理实例,并返回结果

动态代理的好处

+ 可以使真实角色的操作更加纯粹, 不用去关注一些公共的业务
+ 公共的操作交给了代理角色,实现了业务的分工
+ 公共业务发生扩展的时候,方便集中管理
+ 一个动态代理类代理一个接口,一般就是对应的一类业务
+ 一个动态代理类可以代理多个类,只要是实现了同一个接口即可

## 11. AOP

### 1. 什么是AOP

AOP(Aspect Oriented Programming) 意为: 面向切面编程, 通过预编译方式和运行期动态代理实现程序功能的统一维护的一种技术,AOP是OOP的延续,是软件开发中的一个热点,也是Spring框架中的一个重要内容,是函数式编程的一种衍生范型. 利用AOP可以对业务逻辑的各个部分进行隔离,从而使得业务逻辑各部分之间的耦合度降低,提高程序的可重用性,同时提高了开发的效率.

![image-20200819145643549](spring.assets/image-20200819145643549.png)

### 2. AOP在Spring中的作用

==提供声明式事务: 允许用户自定义切面==

+ 横切关注点: 跨越应用程序多个模块的方法或功能,即使,与我们业务逻辑无关的,但是我们需要关注的部分,就是横切关注点. 比如日志,安全,缓存,事务等等.....
+ 切面(aspect): 和企鹅关注点被模块化的特殊对象,即,他是一个类
+ 通知(advice) : 切面必须完成的工作,即,他是类中的一个方法
+ 目标(target) : 被通知的对象
+ 代理(Porxy) : 项目标对象引用通知之后创建的对象.
+ 切入点(pointcut) : 切面通知执行的地点的定义.
+ 连接点(joinpoint) : 与切入点匹配的执行点.

### 3. aop实践

#### 1. 原生Spring API接口

```java
// 接口
public interface Service {
    void add();
    void delete();
    void update();
    void query();
}

// 实现类
public class ServiceImpl implements Service {

    public void add() {
        System.out.println("新增了一个用户!");
    }

    public void delete() {
        System.out.println("删除了一个用户!");
    }

    public void update() {
        System.out.println("修改了一个用户!");
    }

    public void query() {
        System.out.println("查询了一个用户!");
    }
}

// 测试类
public class MyTest {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
        Service userService = (Service) context.getBean("userService");
        userService.add();
    }
}
```

```java
// aop切面类
	// 前置通知
public class Log implements MethodBeforeAdvice {
    public void before(Method method, Object[] objects, Object o) throws Throwable {
        if (o != null) {
            System.out.println(o.getClass().getName() + "的" + method.getName() + "被执行了");
        }
    }
}
	// 后置通知
public class AfterLog implements AfterReturningAdvice {
    public void afterReturning(Object o, Method method, Object[] objects, Object o1) throws Throwable {
        System.out.println("执行了" + method.getName() + "方法,返回结果为" + o);
    }
}
```



```xml

<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:aop="http://www.springframework.org/schema/aop"
       xmlns:context="http://www.springframework.org/schema/context"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
                           http://www.springframework.org/schema/beans/spring-beans-4.0.xsd
                           http://www.springframework.org/schema/aop
                           http://www.springframework.org/schema/aop/spring-aop-4.0.xsd
                           http://www.springframework.org/schema/context
                           https://www.springframework.org/schema/context/spring-context.xsd">

    <!--配置-->
    <bean id="before" class="com.grox.test01.log.Log"/>
    <bean id="after" class="com.grox.test01.log.AfterLog"/>
    <bean id="userService" class="com.grox.test01.service.impl.ServiceImpl"/>


    <!--方式一: 使用原生Spring API接口-->
    <!--配置aop: 需要导入aop的约束-->
    <aop:config>
        <!--切入点: expression:表达式,execution(要执行的位置! * * * * *)-->
        <aop:pointcut id="point1" expression="execution(* com.grox.test01.service.impl.ServiceImpl.*(..))"/>
        <!--执行环绕增加-->
        <aop:advisor advice-ref="before" pointcut-ref="point1"/>
        <aop:advisor advice-ref="after" pointcut-ref="point1"/>

    </aop:config>


</beans>
```

#### 2. 自定义类

```java
自定义类
public class Diy {
    public void before() {
        System.out.println("===================");
    }

    public void after() {
        System.out.println("++++++++++++++++++++");
    }
} 
```

```xml
<!--方式二, 自定义类-->
    <bean id="diy" class="com.grox.test01.log.Diy"/>

    <aop:config>
        <!--自定义切面, ref是要引用的类-->
        <aop:aspect ref="diy">
            <!--切入点-->
            <aop:pointcut id="point" expression="execution(* com.grox.test01.service.impl.*.*(..))"/>
            <!--通知-->
            <aop:before method="before" pointcut-ref="point"/>
            <aop:after method="after" pointcut-ref="point"/>
        </aop:aspect>
    </aop:config>
```

```java
// 测试
public class MyTest {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
        Service userService = (Service) context.getBean("userService");
        userService.add();
    }
}
```

#### 3. 半注解

**1. 切面类**

```java
@Aspect
public class AnnoLog {

    @Before("execution(* com.grox.test01.service.impl.*.*(..))")
    public void before() {
        System.out.println("=====方法执行前======");
    }

    @AfterReturning("execution(* com.grox.test01.service.impl.*.*(..))")
    public void after() {
        System.out.println("=====方法执行后======");
    }
}
```

​	**2.配置文件**

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:aop="http://www.springframework.org/schema/aop"
       xmlns:context="http://www.springframework.org/schema/context"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
                           http://www.springframework.org/schema/beans/spring-beans-4.0.xsd
                           http://www.springframework.org/schema/aop
                           http://www.springframework.org/schema/aop/spring-aop-4.0.xsd
                           http://www.springframework.org/schema/context
                           https://www.springframework.org/schema/context/spring-context.xsd">

    
    <aop:aspectj-autoproxy/>
    <bean id="annoLog" class="com.grox.test01.log.AnnoLog"/>


    <bean id="before" class="com.grox.test01.log.Log"/>
    <bean id="after" class="com.grox.test01.log.AfterLog"/>
    <bean id="userService" class="com.grox.test01.service.impl.ServiceImpl"/>


</beans>
```

​	**3.测试**

```java
public class MyTest {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
        Service userService = (Service) context.getBean("userService");

        userService.add();
    }
}
```

#### 4.全注解

**1. 配置类**

```java
@ComponentScan("com.grox.test01")
@Configuration
@EnableAspectJAutoProxy // 开启 spring 对注解 AOP 的支持
public class AopConfig {

    @Bean
    public Service service() {
        return new ServiceImpl();
    }
}
```

**2. 切面类**

```java
@Aspect  //表示当前类是一个切面类
@Component
public class AnnoLog {

    @Before("execution(* com.grox.test01.service.impl.*.*(..))")
    public void before() {
        System.out.println("=====方法执行前======");
    }

    @AfterReturning("execution(* com.grox.test01.service.impl.*.*(..))")
    public void after() {
        System.out.println("=====方法执行后======");
    }
}
```

**3. 测试**

```java
public class MyTest {
    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(AopConfig.class);
        Service userService = (Service) context.getBean("service");
        userService.add();
    }
}
```



## 12. 整合Mybatis

需要导入包

+ junit
+ mysql
+ mybatis
+ spring-webmvc
+ jdbc
+ aop
+ mybatis-spring

```xml
  <!--junit-->
<!--        <dependency>-->
<!--            <groupId>junit</groupId>-->
<!--            <artifactId>junit</artifactId>-->
<!--            <version>4.13</version>-->
<!--            <scope>test</scope>-->
<!--        </dependency>-->
         <!--mysql-->
        <dependency>
            <groupId>mysql</groupId>
            <artifactId>mysql-connector-java</artifactId>
            <version>5.1.47</version>
        </dependency>
         <!--mybatis-->
        <dependency>
            <groupId>org.mybatis</groupId>
            <artifactId>mybatis</artifactId>
            <version>3.5.4</version>
        </dependency>
         <!--spring-webmvc-->
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-webmvc</artifactId>
            <version>5.2.7.RELEASE</version>
        </dependency>
         <!--jdbc-->
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-jdbc</artifactId>
            <version>5.2.7.RELEASE</version>
        </dependency>
         <!--aop-->
        <dependency>
            <groupId>org.aspectj</groupId>
            <artifactId>aspectjweaver</artifactId>
            <version>1.9.4</version>
        </dependency>
         <!--mybatis-spring-->
        <dependency>
            <groupId>org.mybatis</groupId>
            <artifactId>mybatis-spring</artifactId>
            <version>2.0.4</version>
        </dependency>
```





### 1. mybatis-spring

1. 创建实体类

```java
public class User {
    private Integer id;
    private String name;
    private String sex;

    public User() {
    }

    public User(Integer id, String name, String sex) {
        this.id = id;
        this.name = name;
        this.sex = sex;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", sex='" + sex + '\'' +
                '}';
    }
}
```

2. 创建mapper接口

```java
public interface UserMapper {
    List<User> getUser();
}
```

3. 创建mapper接口的实现,也就是数据库操作

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper
  PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN"
  "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="com.grox.mapper.UserMapper">
  <select id="getUser" resultType="User">
    select * from user
  </select>
</mapper>
```

4. 配置mybatis核心配置

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE configuration
  PUBLIC "-//mybatis.org//DTD Config 3.0//EN"
  "http://mybatis.org/dtd/mybatis-3-config.dtd">
<!--configuration核心配置文件-->
<!--environments 开发环境,可以有多套环境-->
<!--transactionManager 事务管理 默认jdbc--> 
<!--configuration核心配置文件-->
<configuration>
  <!--environments 开发环境,可以有多套环境-->
  <environments default="development">
    <environment id="development">
      <!--transactionManager 事务管理 默认jdbc url的value根据自己的实际数据库环境-->
      <transactionManager type="JDBC"/>
      <dataSource type="POOLED">
        <property name="driver" value="com.mysql.jdbc.Driver"/>
        <property name="url" value="jdbc:mysql://localhost:3306/test?useSSL=true&amp;useUNnicode=true&amp;characterEncoding=UTF-8"/>
        <property name="username" value="root"/>
        <property name="password" value="root"/>
      </dataSource>
    </environment>
  </environments>
<!-- 每一个Mapper.XML都需要在Mybatis的核心配置文件上注册 
	resource的值根据自己的实际路径进行就该-->
  <mappers>
    <mapper resource="com/grox/mapper/UserDao.xml"/>  
  </mappers>
</configuration>
```



4. 之后我们就进入到了mybatis-spring中了,首先加入spring的配置文件

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://www.springframework.org/schema/beans
                      http://www.springframework.org/schema/beans/spring-beans-4.0.xsd">

    <!--DataSource: 适应Spring的数据源替换Mybatis的配置, c3p0 dbcp druid
    我们这里使用Spring提供的JDBC : org.springframework.jdbc.datasource-->
    <bean id="dataSource" class="org.springframework.jdbc.datasource.DriverManagerDataSource">
        <property name="driverClassName" value="com.mysql.jdbc.Driver"/>
        <property name="url" value="jdbc:mysql://localhost:3306/test?useSSL=true&amp;useUNnicode=true&amp;characterEncoding=UTF-8"/>
        <property name="username" value="root"/>
        <property name="password" value="root"/>
    </bean>

    <!--sqlSessionFactory-->
    <bean id="sqlSessionFactory" class="org.mybatis.spring.SqlSessionFactoryBean">
        <property name="dataSource" ref="dataSource"/>
        <!--绑定myabtis配置文件-->
        <property name="configLocation" value="classpath:mybatis-config.xml"/>
        <property name="mapperLocations" value="classpath:com/grox/mapper/*.xml"/>

    </bean>

    <!--sqlsessionTemplate: 就是我们使用的sqlseesion-->
    <bean id="sqlSession" class="org.mybatis.spring.SqlSessionTemplate">
        <constructor-arg index="0" ref="sqlSessionFactory"/>
    </bean>

</beans>
```

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
                      http://www.springframework.org/schema/beans/spring-beans-4.0.xsd">

    <import resource="spring-config.xml"/>

    <bean id="userMapper" class="com.grox.mapper.UserMapperImpl">
        <constructor-arg name="sessionTemplate" ref="sqlSession"/>
    </bean>
</beans>
```





由于mybatis的设置都被spring所托管,所以我们的mybatis核心配置文件就可以退休了

![image-20200824114214858](spring.assets/image-20200824114214858.png)

6. 在spring配置文件中,我们用上了sqlsessionTemplate, 这个配置让我们不必再代码层面反复的创建sqlsession了, 我们可以在拿一个类直接接收它

```java
public class UserMapperImpl implements UserMapper{
    private SqlSessionTemplate sessionTemplate;

    public UserMapperImpl(SqlSessionTemplate sessionTemplate) {
        this.sessionTemplate = sessionTemplate;
    }


    public List<User> getUser() {
        UserMapper mapper = sessionTemplate.getMapper(UserMapper.class);
        return mapper.getUser();
    }
}
```

测试

```java
public class MyTest {
    public static void main(String[] args) throws IOException {
        ApplicationContext context = new ClassPathXmlApplicationContext("application.xml");
        UserMapper bean = context.getBean(UserMapper.class);
        List<User> user = bean.getUser();
        for (User user1 : user) {
            System.out.println("user1 = " + user1);
        }
    }
}
```



但是这样还是有点麻烦,我们还是需要自己手动去创建和接收sqlsession,于是乎,mybatis-spring帮我们又双叒叕省力了

**SqlSessionDaoSupport**

只要继承这个类,我们就再也不用手动配置sqlsession了

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://www.springframework.org/schema/beans
                      http://www.springframework.org/schema/beans/spring-beans-4.0.xsd">

    <!--DataSource: 适应Spring的数据源替换Mybatis的配置, c3p0 dbcp druid
    我们这里使用Spring提供的JDBC : org.springframework.jdbc.datasource-->
    <bean id="dataSource" class="org.springframework.jdbc.datasource.DriverManagerDataSource">
        <property name="driverClassName" value="com.mysql.jdbc.Driver"/>
        <property name="url" value="jdbc:mysql://localhost:3306/test?useSSL=true&amp;useUNnicode=true&amp;characterEncoding=UTF-8"/>
        <property name="username" value="root"/>
        <property name="password" value="root"/>
    </bean>

    <!--sqlSessionFactory-->
    <bean id="sqlSessionFactory" class="org.mybatis.spring.SqlSessionFactoryBean">
        <property name="dataSource" ref="dataSource"/>
        <!--绑定myabtis配置文件-->
        <property name="configLocation" value="classpath:mybatis-config.xml"/>
        <property name="mapperLocations" value="classpath:com/grox/mapper/*.xml"/>

    </bean>

 
    <!--sqlsessionTemplate: 就是我们使用的sqlseesion-->
<!--    <bean id="sqlSession" class="org.mybatis.spring.SqlSessionTemplate">-->
<!--        <constructor-arg index="0" ref="sqlSessionFactory"/>-->
<!--    </bean>-->

</beans>
```



```java
public class UserMapperImpl2 extends SqlSessionDaoSupport implements UserMapper{
    public List<User> getUser() {
        return getSqlSession().getMapper(UserMapper.class).getUser();
    }
}
```





```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
                      http://www.springframework.org/schema/beans/spring-beans-4.0.xsd">

    <import resource="spring-config.xml"/>


    <bean id="userMapper2" class="com.grox.mapper.UserMapperImpl2">
        <property name="sqlSessionFactory" ref="sqlSessionFactory"/>
    </bean>

</beans>
```

测试

```java
public class MyTest {
    @Test
    public void test01() {
        ApplicationContext context = new ClassPathXmlApplicationContext("application.xml");
        UserMapper userMapper2 = (UserMapper) context.getBean("userMapper2");
        List<User> user = userMapper2.getUser();
        for (User user1 : user) {
                    System.out.println("user1 = " + user1);
                }
    }
}
```



### 2. 事务

一个使用 MyBatis-Spring 的其中一个主要原因是它允许 MyBatis 参与到 Spring 的事务管理中。而不是给 MyBatis 创建一个新的专用事务管理器，MyBatis-Spring 借助了 Spring 中的 DataSourceTransactionManager 来实现事务管理。

一旦配置好了 Spring 的事务管理器，你就可以在 Spring 中按你平时的方式来配置事务。并且支持 @Transactional 注解和 AOP 风格的配置。在事务处理期间，一个单独的 `SqlSession` 对象将会被创建和使用。当事务完成时，这个 session 会以合适的方式提交或回滚。

事务配置好了以后，MyBatis-Spring 将会透明地管理事务。这样在你的 DAO 类中就不需要额外的代码了。

spring中的事务配置分为两种: 编程式事务和声明式事务

#### 1. 编程式事务

编程式事务的最大特点是它需要修改原来的代码,高侵入性.  所以我们不建议使用这一类的事务操作

```java
TransactionStatus txStatus =
    transactionManager.getTransaction(new DefaultTransactionDefinition());
try {
  userMapper.insertUser(user);
} catch (Exception e) {
  transactionManager.rollback(txStatus);
  throw e;
}
transactionManager.commit(txStatus);
```



#### 2. 声明式事务

声明式事务是将事务在配置文件中设置好,然后由aop切入到源代码中,不会源代码进行任何修改,非侵入性

```xml
    
<!--配置声明式事务-->
    <bean id="transactionManager" class="org.springframework.jdbc.datasource.DataSourceTransactionManager">
        <property name="dataSource" ref="dataSource"/>
    </bean>

    <!--结合aop实现事务的植入-->
    <!--配置事务通知-->
    <tx:advice id="transaction" transaction-manager="transactionManager">
        <tx:attributes>
            <tx:method name="*" propagation="REQUIRED"/>
        </tx:attributes>
    </tx:advice>

    <aop:config>
        <aop:pointcut id="txpointcut" expression="execution(* com.grox.mapper.*.*(..))"/>
        <aop:advisor advice-ref="transaction" pointcut-ref="txpointcut"/>
    </aop:config>
```

























































































