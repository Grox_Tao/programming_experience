package com.sgcc.sgga.tools;

/*
 * @ClassName InputInjectFilter
 * @CNname 类型转换
 * @Author Grox
 * @Date 2020/6/22 10:36 周一
 * @Version 1.0
 */

import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import org.springframework.web.util.HtmlUtils;

import java.util.Iterator;
import java.util.Set;

public class InputInjectFilter {
    public static Object encodeInputString(Object obj, Class clz) {
        if (obj != null) {
            JSONObject jsObject = encodeInputString(JSONUtil.parseObj(obj));
            return JSONUtil.toBean(jsObject, clz);
        } else {
            return null;
        }
    }

    public static JSONObject encodeInputString(JSONObject jsObject) {
        if (jsObject != null) {
            Set<String> entrySet = jsObject.keySet();
            Iterator iterator = entrySet.iterator();

            while (true) {
                while (iterator.hasNext()) {
                    String key = (String) iterator.next();
                    if (jsObject.get(key) != null && jsObject.get(key) instanceof String) {
                        jsObject.put(key, encodeInputString((String) jsObject.get(key)));
                    } else if (jsObject.get(key) != null && jsObject.get(key) instanceof JSONArray) {
                        jsObject.put(key, encodeInputString(jsObject.getJSONArray(key)));
                    } else if (jsObject.get(key) != null && jsObject.get(key) instanceof JSONObject) {
                        jsObject.put(key, encodeInputString(jsObject.getJSONObject(key)));
                    }
                }

                return jsObject;
            }
        } else {
            return jsObject;
        }
    }

    public static String encodeInputString(String input) {
        String resut = HtmlUtils.htmlEscape(input);
        return resut;
    }

    public static JSONArray encodeInputString(JSONArray jsonArray) {
        for (int i = 0; i < jsonArray.size(); ++i) {
            if (jsonArray.get(i) != null && jsonArray.get(i) instanceof String) {
                jsonArray.set(i, encodeInputString((String) jsonArray.get(i)));
            } else if (jsonArray.get(i) != null && jsonArray.get(i) instanceof JSONObject) {
                encodeInputString(jsonArray.getJSONObject(i));
            }
        }

        return jsonArray;
    }
}
