# DO,DTO,VO,POJO你都知道吗

## DO(Data Object)

与数据表结构一一对应,通过DAO层向上传输数据源对象.

## PO(Persistant Object)

持久对象,一个PO的数据结构对应着库中表的结构,表中的一个记录就是一个PO对象

## DTO(Data Transfer Object)

数据传输对象,Service和Manager向外传输的对象

## BO(Business Object)

业务对象。由Service层输出的封装业务逻辑的对象

## AO(Application Object) 

应用对象，在Web层与Service层之间抽象的复用对象模型，极为贴近展示层，复用度不高。

## VO(View Object)

显示层对象，通常是Web向模板渲染引擎层传输的对象

## POJO（Plain Ordinary Java Object）

POJO专指只有setter/getter/tostring的简单类，包括DO/DTO/BO/VO

## DAO（Data Access Object）

数据访问对象，和上面的O不同的是，其功能是用于进行数据操作的。通常不会用于描述数据实体。

![image-20200729092415177](DO,DTO,VO,POJO你都知道吗.assets/image-20200729092415177.png)

我们知道，一般情况下，前端是不会凭空造出数据的，因此最后前端展示的数据一定是从数据库中来的，数据的流向通常也是数据可流向页面。我将其分为三个部分：数据访问、业务处理和业务解释。

1. 数据访问：这一部分是用于从数据库中读取数据，将数据记录转换成和数据实体也就是java对象，便于操作。
2. 业务处理：这一部分是数据流的核心，几乎所有数据的操作都是在这一部分完成的。
3. 业务解释：这一部分是用于展示给前端的数据，解释业务体现在某些字段/值是需要经过处理的才会呈现的。



**关键点**

+ DAO，用于操作数据而不是描述数据的
+ PO/DO/Entity，其数据结构对应数据表中的一条记录，因此是同一类别的。
+ BO，可以理解为PO是一条交易记录，BO就可以是一个人全部的交易记录集合对象。
+ DTO，用于传输数据，可能传递给前端，也有可能传递给其他系统。用于承载数据。
+ VO，前端最后需要的数据长什么样，对应的对象就是VO