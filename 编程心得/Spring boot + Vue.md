# Spring boot + Vue

## 1. 用命令创建一个Maven工程的方法

```
mvn archetype:generate -DgroupId=com.grox -DartifactId=test01 -DarchetypeArtifactId =maven-archetype-quickstart -DinteractiveMode=false
```

+ -DgroupId **:** 组织id(项目包名)
+ -DartifactId **:** ArtifactId(项目名称或者更模块名称)
+ -DarchetypeArtifactId **:** 项目骨架
+ -DinteractiveMode **:** 是否使用交互模式

## 2. 添加依赖

+ 首先添加spring-boot-starter-parent 作为praent

```xml
<parent>
	<groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-parent</artifactId>
    <version>2.0.4.RELEASE</version>
</parent>
```

## 3. 启动类注解解释

>@EnableAutoConfiguration : 表示开启自动化配置
>
>@ComponentScan : 进行包扫描,将控制器(Controller)注册到SpringMVC容器中
>
>@SpringBootApplication : 组合注解,功能等同于上面的综合

## 4. 项目启动

方法一

```shell
mvn spring-boot:run
```

方法二

​	直接在IDE中运行启动类的main方法

方法三 : 打包启动

1. 直接将springboot应用打成jar包,首先需要添加一个插件(plugin)到pom.xml中

```xml
<build>
	<plugins>
    	<plugin>
        	<groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-maven-plugin</artifactId>
        </plugin>
    </plugins>
</build>
```

2. 然后运行mvn命令进行打包

```
mvn package
```

3. 打包完成后,会在target目录下生成一个jar文件,通过java -jar 命令直接启动这个jar文件
4.  ........==等待看本书第十五章==

`

## 5. springboot基本配置

dependencyManagement : 依赖管理,会在顶级父工程中出现,起到了项目依赖统一管理的功能

```xml
// 依赖的版本
<properties>
    	<!--这个配置是用来配置编码格式的-->
    	<!--当然,如果你是用IDE生成的运用到初始化的maven生成,就不需要配置了,默认加上-->
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
        <junit.version>4.11</junit.version>
        
    </properties>

	<!--依赖管理工具-->
    <dependencyManagement>
        <dependencies>
            <!--junit-->
            <dependency>
                <groupId>junit</groupId>
                <artifactId>junit</artifactId>
                <version>${junit.version}</version> 从properties抽取版本
                <scope>test</scope>
            </dependency>
    </dependencyManagement>
```

有了上面的配置,就不在需要继承spring-boot-starter-parent了,但是java的版本就需要开发者手动配置了,添加起来也很简单,加一个plugin就可以了

```xml
<plugin>
	<groupId>org.apache.maven.plugins</groupId>
    <artifactId>maven-compiler-plugin</artifactId>
    <version>3.1</version>
    <configuration>
    	<source>1.8</source>
        <target>1.8</target>
    </configuration>
</plugin>
```

