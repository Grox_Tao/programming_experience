## Java操作Excel

#### 操作Excel比较流行

​	poi(Apache)、easyExcel(阿里巴巴)、easypoi(悟耘)、excelWriter(hutool)

##### POI

![Snipaste_2020-05-16_22-10-08](\Users/grox/code/suibi/programming_experience/pic\Snipaste_2020-05-16_22-10-08.png)

***HSSF 03 .xls    |   XSSF 07 xlsx***

EasyExcel是阿里巴巴开源的一个excel处理框架,**以使用简单,节省内存著称**

EasyExcel能大大减少占用内存的主要原因是在解析Excel时没有将文件数据一次性全部加载到内存中,而是从磁盘上一行行读取数据,逐个解析.

##### 内存解析

 	1. POI   如果有100W行数据,会一次性加载到内存中,会出现内存溢出(OOM)的问题.
 	2. easyExcel  如果有100W行数据,会一行一行的写入;

EasyExcel和POI在解析Excel时的对比图

![Snipaste_2020-05-16_22-12-59](/Users/grox/code/suibi/programming_experience/pic\Snipaste_2020-05-16_22-12-59.png)

##### POI主要对象

1. 工作簿
2. 工作表 sheet
3. 行
4. lie

![Snipaste_2020-05-16_23-31-18](/Users/grox/code/suibi/programming_experience/pic\Snipaste_2020-05-16_23-31-18.png)

![Snipaste_2020-05-16_23-32-00](/Users/grox/code/suibi/programming_experience/pic\Snipaste_2020-05-16_23-32-00.png)



大文件写HSSF

缺点:最多只能处理65536行数据,否则会抛异常

优点: 过程写入缓存,不操作磁盘,最后一次性写入磁盘,速度快

![Snipaste_2020-05-16_23-33-52](/Users/grox/code/suibi/programming_experience/pic\Snipaste_2020-05-16_23-33-52.png)

大文件写XSSF

缺点: 写数据时速度非常慢,非常耗内存,也会发生内存溢出,如100万条

优点: 可以写较大的数据量,如20万条

