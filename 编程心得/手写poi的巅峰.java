
import cn.hutool.core.io.IoUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.poi.excel.ExcelUtil;
import cn.hutool.poi.excel.ExcelWriter;
import com.sgcc.sgga.daily.model.BenchangFeixing;
import com.sgcc.sgga.daily.model.Car;
import com.sgcc.sgga.daily.excel.DailyExcel.List.Lists;
import com.sgcc.sgga.daily.excel.DailyExcel.List.ParamsList;
import com.sgcc.sgga.daily.service.impl.DailyServiceImpl;
import com.sgcc.sgga.employe.model.EmployeVO;
import com.sgcc.sgga.employe.service.impl.EmployeServiceImpl;

import com.sgcc.sgga.loginuser.LoginUser;
import com.ylkj.cloud.base.model.UserVO;
import com.ylkj.cloud.core.response.Result;
import com.ylkj.cloud.core.response.ResultGenerator;
import com.ylkj.cloud.core.utils.RedisTemplateUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/*
 * @ClassName DailyExcel
 * @CNname 生产日报导出
 * @Author Grox
 * @Date 2020/4/24 18:21
 * @Version 1.0
 */
@CrossOrigin
@Slf4j
@RestController
@RequestMapping("/dailyxls")
public class DailyExcel {
    @Autowired
    private RedisTemplateUtil redisUtil;
    @Autowired
    private EmployeServiceImpl employeService;
    @Autowired
    private DailyServiceImpl dailyServiceImpl;

    @GetMapping("/getbenchang/{stime}/{etime}")
    public Result getBenchangfeixing(HttpServletRequest request, HttpServletResponse response, @PathVariable("stime") String stime, @PathVariable("etime") String etime) {
        // 获取登录人信息
        UserVO loginUserInfo = LoginUser.getLoginUserInfo(redisUtil);
        // 调用人员表获取人员信息
        EmployeVO getcomIdandEmId =
                employeService.getcomIdandEmId(loginUserInfo.getUsername());
        // 获取登录人分公司id
        Integer fkBsCmmCompanyId = getcomIdandEmId.getFkBsCmmCompanyId();
        if (LoginUser.isBenBu(fkBsCmmCompanyId)) {
            fkBsCmmCompanyId = null;
        }
        // 判断分公司名称
        String CompanyName = (fkBsCmmCompanyId == 7101 ? "北京分公司" : (fkBsCmmCompanyId == 7102 ? "华中分公司" :
                (fkBsCmmCompanyId == 7103 ? "华东分公司" : "")));
        ExcelWriter writer = null;
        ServletOutputStream out = null;
        // 保障内容集合
        Map<String, String> context = null;
        // 列号
        Integer key = null;
        Integer column = null;
        // 做行数标记
        int flag = 0; // flag = 0

        try {
            writer = ExcelUtil.getWriter();
            // 设置自动换行
            // StyleSet styleSet = writer.getStyleSet();
            // styleSet.setWrapText();
            // 设置时间
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            String sDate = sdf.format(this.getSDate(stime));
            String eDate = sdf.format(this.getSDate(etime));
            // 开始导表
            // 列表抬头
            writer.merge(8, CompanyName + "保障数据统计日报表" + " (时间: " + sDate + "--" + eDate + ")");
            writer.merge(1, 1, 0, 5, "01值班：", false);
            writer.merge(1, 1, 6, 8, "填报人：", false);

            // ====================================================================================
            writer.merge(2, 2, 0, 8, "表1：本场飞行保障情况", false);
            // 设置行
            int row = writer.getCurrentRow() + 2; // 3
            // 表头
            Map<Integer, String> lieBiao = Lists.LieBiao();
            for (int x = row; x < row + 1; x++) {
                for (int y = 0; y < lieBiao.size(); y++) {
                    String value = lieBiao.get(y);
                    writer.writeCellValue(y, x, value); // 参数一为列 参数二为行
                }
            }
            row = row + 1; // 4
            // 本场飞行保障情况
            // 获取车辆类型集合
            List<String> carType = new LinkedList<String>();
            // 获取车牌号集合
            List<String> carNum = new LinkedList<String>();
            // 获取机组号集合
            List<String> jizuNum = new LinkedList<String>();
            // 获取序号集合
            List<String> index = new LinkedList<String>();
            // 获取本场飞行保障情况数据
            BenchangFeixing benchangfeixing = dailyServiceImpl.getBenchangfeixing(stime, etime, fkBsCmmCompanyId);
            // 获取车辆数据
            LinkedList<Car> carList = benchangfeixing.getCarList();
            // 遍历车辆数据
            for (Car car : carList) {
                // 给车辆类型集合/车牌号集合/机组号集合/序号集合分别添加数据
                carType.add(car.getCarType());
                carNum.add(car.getCarNumber());
                jizuNum.add(car.getJizuNumber());
                index.add(car.getIndex());
            }
            // 索引(此索引目的是参与保障内容具体数据的获取)
            int temp = 0;
            // 遍历车辆类型集合
            for (String type : carType) {
                // 油车
                if (Objects.equals(type, "油车")) {
                    // 保障内容
                    // 获取 '保障内容' 所在的列号
                    key = DailyExcel.getKey(lieBiao, "保障内容");
                    column = key; // 5
                    // 设置临时列号
                    int tempcolumn = column;
                    // 获取油车保障内容抬头
                    List<String> baoZhangNeiRong = Lists.BaoZhangNeiRong();
                    for (int y = tempcolumn; y < tempcolumn + 1; y++) {
                        // 设置关键点(目的是为了在输出'行驶总里程数（公里）' 时合并架次,小时栏)
                        int spot = 0;
                        // 输出具体的保障内容项
                        for (int x = row; x < row + 5; x++) {
                            String value = baoZhangNeiRong.get(spot);
                            // 给保障内容赋值 参数一为当前保障内容项,参数二为序号
                            context = new ParamsList().getContext(value, index.get(temp), benchangfeixing);
                            writer.writeCellValue(y, x, value); // 参数一为列 参数二为行
                            // 备注
                            String Beuzhu = context.get("Beuzhu");
                            if (ObjectUtil.isNotEmpty(Beuzhu)) {
                                key = DailyExcel.getKey(lieBiao, "备注"); // 从返回数组获取
                                column = key; // 8
                                writer.writeCellValue(column, x, Beuzhu);
                            } else {
                                key = DailyExcel.getKey(lieBiao, "备注"); // 从返回数组获取
                                column = key; // 8
                                writer.writeCellValue(column, x, "");
                            }
                            if (spot != 4) {
                                // 小时
                                String Xiaoshi = context.get("Xiaoshi");
                                if (ObjectUtil.isNotEmpty(Xiaoshi)) {
                                    key = DailyExcel.getKey(lieBiao, "小时"); // 从返回数组获取
                                    column = key; // 7
                                    writer.writeCellValue(column, x, Xiaoshi);
                                } else {
                                    key = DailyExcel.getKey(lieBiao, "小时"); // 从返回数组获取
                                    column = key; // 7
                                    writer.writeCellValue(column, x, "");
                                }
                                // 架次
                                String Jiaci = context.get("Jiaci");
                                if (ObjectUtil.isNotEmpty(Jiaci)) {
                                    key = DailyExcel.getKey(lieBiao, "架次"); // 从返回数组获取
                                    column = key; // 6
                                    writer.writeCellValue(column, x, Jiaci);
                                } else {
                                    key = DailyExcel.getKey(lieBiao, "架次"); // 从返回数组获取
                                    column = key; // 6
                                    writer.writeCellValue(column, x, "");
                                }
                            } else {
                                // 架次
                                String Jiaci = context.get("Jiaci");
                                if (ObjectUtil.isNotEmpty(Jiaci)) {
                                    key = DailyExcel.getKey(lieBiao, "架次"); // 从返回数组获取
                                    column = key; // 6
                                    writer.merge(x, x, column, column + 1, Jiaci, false);
                                } else {
                                    key = DailyExcel.getKey(lieBiao, "架次"); // 从返回数组获取
                                    column = key; // 6
                                    writer.merge(x, x, column, column + 1, "", false);
                                }
                            }
                            spot++;
                        }
                    }
                    // 车号
                    if (ObjectUtil.isNotEmpty(carNum.get(temp))) {
                        key = DailyExcel.getKey(lieBiao, "车号");
                        column = key; // 4
                        writer.merge(row, row + 4, column, column, carNum.get(temp), false); // 从返回数组获取
                    } else {
                        key = DailyExcel.getKey(lieBiao, "车号");
                        column = key; // 4
                        writer.merge(row, row + 4, column, column, "", false); // 从返回数组获取
                    }
                    // 机组号
                    if (ObjectUtil.isNotEmpty(jizuNum.get(temp))) {
                        key = DailyExcel.getKey(lieBiao, "机组号");
                        column = key; // 3
                        writer.merge(row, row + 4, column, column, jizuNum.get(temp), false); // 为空
                    } else {
                        key = DailyExcel.getKey(lieBiao, "机组号");
                        column = key; // 3
                        writer.merge(row, row + 4, column, column, "", false); // 为空
                    }
                    // 序号
                    if (ObjectUtil.isNotEmpty(index.get(temp))) {
                        key = DailyExcel.getKey(lieBiao, "序号");
                        column = key; // 2
                        writer.merge(row, row + 4, column, column, index.get(temp), false); // 从返回数组获取
                    } else {
                        key = DailyExcel.getKey(lieBiao, "序号");
                        column = key; // 2
                        writer.merge(row, row + 4, column, column, "", false); // 从返回数组获取
                    }
                    // 车型
                    if (ObjectUtil.isNotEmpty(type)) {
                        key = DailyExcel.getKey(lieBiao, "车型");
                        column = key; // 1
                        writer.merge(row, row + 4, column, column, type, false); // 从返回数组获取
                    } else {
                        key = DailyExcel.getKey(lieBiao, "车型");
                        column = key; // 1
                        writer.merge(row, row + 4, column, column, "", false); // 从返回数组获取
                    }

                    temp++;
                } else if (Objects.equals(type, "保障车")) {
                    // 获取'保障内容'所在的列号
                    key = DailyExcel.getKey(lieBiao, "保障内容");
                    // 给列赋值
                    column = key; // 5
                    // 设置临时列号
                    int tempcolumn = column;
                    // 获取  保障车 保障内容
                    List<String> baoZhangNeiRong = Lists.BaoZhangNeiRong2();
                    for (int y = tempcolumn; y < tempcolumn + 1; y++) {
                        for (int x = row; x < row + 1; x++) {
                            String value = baoZhangNeiRong.get(0);
                            writer.writeCellValue(y, x, value); // 参数一为列 参数二为行 参数三为具体保障内容
                            // 给保障内容数据赋值 参数一为当前保障内容,参数二为序号(通过序号查一辆车的完整信息),参数三为本场飞行保障情况数据
                            context = new ParamsList().getContext(value, index.get(temp), benchangfeixing);
                            // 备注
                            String Beuzhu = context.get("Beuzhu");
                            if (Beuzhu != null) {
                                key = DailyExcel.getKey(lieBiao, "备注"); // 从返回数组获取
                                column = key; // 8
                                writer.writeCellValue(column, x, Beuzhu);
                            } else {
                                key = DailyExcel.getKey(lieBiao, "备注"); // 从返回数组获取
                                column = key; // 8
                                writer.writeCellValue(column, x, "");
                            }
                            // 架次
                            String Jiaci = context.get("Jiaci");
                            if (Jiaci != null) {
                                key = DailyExcel.getKey(lieBiao, "架次"); // 从返回数组获取
                                column = key; // 6
                                writer.merge(x, x, column, column + 1, Jiaci, false);
                            } else {
                                key = DailyExcel.getKey(lieBiao, "架次"); // 从返回数组获取
                                column = key; // 6
                                writer.merge(x, x, column, column + 1, "", false);
                            }

                        }
                    }

                    // 车号
                    key = DailyExcel.getKey(lieBiao, "车号");
                    column = key; // 4
                    writer.writeCellValue(column, row, carNum.get(temp)); // 从返回数组获取
                    // 机组号
                    key = DailyExcel.getKey(lieBiao, "机组号");
                    column = key; // 3
                    writer.writeCellValue(column, row, jizuNum.get(temp)); // 从返回数组获取
                    // 序号
                    key = DailyExcel.getKey(lieBiao, "序号");
                    column = key; // 2
                    writer.writeCellValue(column, row, index.get(temp)); // 从返回数组获取
                    // 车型
                    key = DailyExcel.getKey(lieBiao, "车型");
                    column = key; // 1
                    writer.writeCellValue(column, row, type); // 从返回数组获取
                    temp++;
                }
                if ("油车".equals(type)) {
                    row += 5;
                } else if ("保障车".equals(type)) {
                    row += 1;
                }
            }
            // 合并表头(纵)
            key = DailyExcel.getKey(lieBiao, "事项");
            column = key; // 0
            writer.merge(4, row - 1, column, column, "本场飞行保障", false);
            // ====================================================================================
            System.out.println("当前的行数1  " + row); //10
            // ====================================================================================

            // 表2：外出机组加油车情况
            writer.merge(row + 1, row + 1, 0, 8, "表2：外出机组加油车情况", false);
            row += 2; // 12
            // 表头
            Map<Integer, String> lieBiao2 = Lists.LieBiao2();
            for (int x = row; x < row + 1; x++) {
                for (int y = 0; y < lieBiao2.size(); y++) {
                    String value = lieBiao2.get(y);
                    writer.writeCellValue(y, x, value); // 参数一为列 参数二为行
                }
            }
            row += 1; // 13
            // 做行数标记
            flag = row; // flag = 13
            // List<String> jiZuHao = Lists.JiZuHao();
            // 调用日报的 '获得时间段内外出机组油车情况' 方法
            BenchangFeixing todayoutyouche = dailyServiceImpl.todayoutyouche(stime, etime, fkBsCmmCompanyId);
            log.info("carType = " + carType.size() + ",carNum = " + carNum.size() + ",jizuNum = " + jizuNum.size() + ",index = " + index.size());
            // 清除所有集合中的数据,为表二做准备
            carType.clear();
            carNum.clear();
            jizuNum.clear();
            index.clear();
            log.info("carType = " + carType.size() + ",carNum = " + carNum.size() + ",jizuNum = " + jizuNum.size() + ",index = " + index.size());
            // 获取'获得时间段内外出机组油车情况'的车辆信息
            LinkedList<Car> todayoutyouchecarList = todayoutyouche.getCarList();
            // 遍历车辆信息
            for (Car car : todayoutyouchecarList) {
                // 给车辆类型集合/车牌号集合/机组号集合/序号集合分别添加数据
                carType.add(car.getCarType());
                carNum.add(car.getCarNumber());
                jizuNum.add(car.getJizuNumber());
                index.add(car.getIndex());
            }
            log.info("carType = " + carType.size() + ",carNum = " + carNum.size() + ",jizuNum = " + jizuNum.size() + ",index = " + index.size());
            log.info("carType = " + carType + ",carNum = " + carNum + ",jizuNum = " + jizuNum + ",index = " + index);
            // 索引
            temp = 0;
            // 遍历机组号
            for (String jizuhao : jizuNum) {
                // 获取'保障内容'所在的列号
                key = DailyExcel.getKey(lieBiao2, "保障内容");
                // 赋值到列
                column = key; // 5
                // 设置临时列号
                int tempcolumn = column;
                // 获取 外出机组车辆保障油车保障内容
                List<String> baoZhangNeiRong3 = Lists.BaoZhangNeiRong3();
                for (int y = tempcolumn; y < tempcolumn + 1; y++) {
                    // 设置关键点tag,用于获取外出机组车辆保障油车保障内容项
                    int tag = 0;
                    for (int x = row; x < row + 3; x++) {
                        String value = baoZhangNeiRong3.get(tag);
                        writer.writeCellValue(y, x, value); // 参数一为列 参数二为行
                        // 给保障内容赋值 参数一为当前保障内容项,参数二为序号(用来查询一辆车的具体信息),参数三是外出机组加油车情况的数据
                        context = new ParamsList().getContext(value, index.get(temp), todayoutyouche);
                        if (tag != 2) {
                            // 次
                            String Xiaoshi = context.get("Xiaoshi");
                            if (Xiaoshi != null) {
                                key = DailyExcel.getKey(lieBiao2, "次"); // 从返回数组获取
                                column = key; // 7
                                writer.writeCellValue(column, x, Xiaoshi);
                            } else {
                                key = DailyExcel.getKey(lieBiao2, "次"); // 从返回数组获取
                                column = key; // 7
                                writer.writeCellValue(column, x, "");
                            }
                            // 升
                            String Jiaci = context.get("Jiaci");
                            if (Jiaci != null) {
                                key = DailyExcel.getKey(lieBiao2, "升"); // 从返回数组获取
                                column = key; // 6
                                writer.writeCellValue(column, x, Jiaci);
                            } else {
                                key = DailyExcel.getKey(lieBiao2, "升"); // 从返回数组获取
                                column = key; // 6
                                writer.writeCellValue(column, x, "");
                            }
                        } else {
                            // 升
                            String Jiaci = context.get("Jiaci");
                            if (Jiaci != null) {
                                key = DailyExcel.getKey(lieBiao2, "升"); // 从返回数组获取
                                column = key; // 6
                                writer.merge(x, x, column, column + 1, Jiaci, false);
                            } else {
                                key = DailyExcel.getKey(lieBiao2, "升"); // 从返回数组获取
                                column = key; // 6
                                writer.merge(x, x, column, column + 1, "", false);
                            }
                        }
                        tag++;
                    }
                }
                context = new ParamsList().getContext(baoZhangNeiRong3.get(0), index.get(temp), todayoutyouche);
                // 司机
                String Beuzhu = context.get("Beuzhu");
                if (Beuzhu != null) {
                    key = DailyExcel.getKey(lieBiao2, "司机"); // 从返回数组获取
                    column = key; // 8
                    writer.merge(row, row + 2, column, column, Beuzhu, false);
                } else {
                    key = DailyExcel.getKey(lieBiao2, "司机"); // 从返回数组获取
                    column = key; // 8
                    writer.merge(row, row + 2, column, column, "", false);
                }
                // 车号
                key = DailyExcel.getKey(lieBiao2, "车号");
                column = key; // 4
                writer.merge(row, row + 2, column, column, carNum.get(temp), false); // 从返回数组获取
                // 机组号
                key = DailyExcel.getKey(lieBiao2, "机组号");
                column = key; // 3
                writer.merge(row, row + 2, column, column, jizuhao, false); // 从返回数组获取
                // 序号
                key = DailyExcel.getKey(lieBiao2, "序号");
                column = key; // 2
                writer.merge(row, row + 2, column, column, index.get(temp), false); // 从返回数组获取
                row += 3;
                temp++;
            }
            // 车型
            key = DailyExcel.getKey(lieBiao2, "车型");
            column = key; // 1
            row = flag;
            int endrow = (row + (index.size() * 3) + 3) - 1;
            writer.merge(row, endrow, column, column, "油车", false); // 从返回数组获取
            // 总计
            // 设置开始列/结束列
            key = DailyExcel.getKey(lieBiao2, "序号");
            int scolumn = key; // 2
            int ecolumn = scolumn + 2;
            // 设置开始行/结束行
            int srow = endrow - 2;
            writer.merge(srow, endrow, scolumn, ecolumn, "总计", false); // 从返回数组获取
            // 总计保障内容
            key = DailyExcel.getKey(lieBiao2, "保障内容");
            column = key; // 5
            int tempcolumn = column;
            List<String> baoZhangNeiRong3 = Lists.BaoZhangNeiRong3();
            // 获取总计中的数据,没有车辆信息
            for (int y = tempcolumn; y < tempcolumn + 1; y++) {
                // 设置关键点,
                int tag = 0;
                for (int x = srow; x < srow + 3; x++) {
                    String value = baoZhangNeiRong3.get(tag);
                    writer.writeCellValue(y, x, value); // 参数一为列 参数二为行
                    // 给保障内容赋值 参数一为当前保障内容名称,参数二为油车外出数据
                    context = new ParamsList().getzongji(value, todayoutyouche);
                    // 司机
                    String Beuzhu = context.get("Beuzhu");
                    if (Beuzhu != null) {
                        key = DailyExcel.getKey(lieBiao2, "司机"); // 从返回数组获取
                        column = key; // 8
                        writer.writeCellValue(column, x, Beuzhu);
                    } else {
                        key = DailyExcel.getKey(lieBiao2, "司机"); // 从返回数组获取
                        column = key; // 8
                        writer.writeCellValue(column, x, "");
                    }
                    if (tag != 2) {
                        // 次
                        String Xiaoshi = context.get("Xiaoshi");
                        if (Xiaoshi != null) {
                            key = DailyExcel.getKey(lieBiao2, "次"); // 从返回数组获取
                            column = key; // 7
                            writer.writeCellValue(column, x, Xiaoshi);
                        } else {
                            key = DailyExcel.getKey(lieBiao2, "次"); // 从返回数组获取
                            column = key; // 7
                            writer.writeCellValue(column, x, "");
                        }
                        // 升
                        String Jiaci = context.get("Jiaci");
                        if (Jiaci != null) {
                            key = DailyExcel.getKey(lieBiao2, "升"); // 从返回数组获取
                            column = key; // 6
                            writer.writeCellValue(column, x, Jiaci);
                        } else {
                            key = DailyExcel.getKey(lieBiao2, "升"); // 从返回数组获取
                            column = key; // 6
                            writer.writeCellValue(column, x, "");
                        }
                    } else {
                        // 升
                        String Jiaci = context.get("Jiaci");
                        if (Jiaci != null) {
                            key = DailyExcel.getKey(lieBiao2, "升"); // 从返回数组获取
                            column = key; // 6
                            writer.merge(x, x, column, column + 1, Jiaci, false);
                        } else {
                            key = DailyExcel.getKey(lieBiao2, "升"); // 从返回数组获取
                            column = key; // 6
                            writer.merge(x, x, column, column + 1, "", false);
                        }
                    }
                    tag++;
                }
            }
            // 合并表头(纵)
            key = DailyExcel.getKey(lieBiao, "事项");
            column = key; // 0
            writer.merge(row, endrow, column, column, "外出机组加油车", false); // 从返回数组获取
            // ====================================================================================
            System.out.println("当前的行数2  " + row); // 23
            // ====================================================================================
            row = endrow + 2; // 54
            System.out.println("当前的行数3  " + row); // 54
            // 表3：外出机组保障车情况
            writer.merge(row, row, 0, 8, "表3：外出机组保障车情况", false);
            // 表头
            row += 1; // 55
            Map<Integer, String> lieBiao3 = Lists.LieBiao3();
            for (int x = row; x < row + 1; x++) {
                for (int y = 0; y <= lieBiao3.size(); y++) {
                    String value = lieBiao3.get(y);
                    if (y == 6) {
                        writer.merge(x, x, y, y + 1, value, false);
                        y += 1;
                    } else {
                        writer.writeCellValue(y, x, value); // 参数一为列 参数二为行
                    }
                }
            }
            row += 1; // 56
            flag = row; // flag = 56
            // 设置索引
            temp = 0;
            // 调用日报 '获得时间段内外出机组保障车情况' 获取外出机组保障车情况数据
            BenchangFeixing todayoutbaozhang = dailyServiceImpl.todayoutbaozhang(stime, etime, fkBsCmmCompanyId);
            log.info("carType = " + carType.size() + ",carNum = " + carNum.size() + ",jizuNum = " + jizuNum.size() + ",index = " + index.size());
            // 清除数据,为表三做准备
            carType.clear();
            carNum.clear();
            jizuNum.clear();
            index.clear();
            log.info("carType = " + carType.size() + ",carNum = " + carNum.size() + ",jizuNum = " + jizuNum.size() + ",index = " + index.size());
            // 获取车辆信息
            LinkedList<Car> todayoutbaozhangcarList = todayoutbaozhang.getCarList();
            // 遍历车辆信息
            for (Car car : todayoutbaozhangcarList) {
                // 给车辆类型集合/车牌号集合/机组号集合/序号集合分别添加数据
                carType.add(car.getCarType());
                carNum.add(car.getCarNumber());
                jizuNum.add(car.getJizuNumber());
                index.add(car.getIndex());
            }
            log.info("carType = " + carType.size() + ",carNum = " + carNum.size() + ",jizuNum = " + jizuNum.size() + ",index = " + index.size());
            // 遍历机组号
            for (String jizuhao : jizuNum) {
                // 获取'保障内容'内容所在的列号
                key = DailyExcel.getKey(lieBiao3, "保障内容");
                column = key; // 5
                // 设置临时列表
                tempcolumn = column;
                // 获取'外出机组车辆保障' 保障内容项
                List<String> baoZhangNeiRong = Lists.BaoZhangNeiRong2();
                for (int y = tempcolumn; y < tempcolumn + 1; y++) {
                    // 设置关键点
                    int tag = 0;
                    for (int x = row; x < row + 1; x++) {
                        String value = baoZhangNeiRong.get(0);
                        writer.writeCellValue(y, x, value); // 参数一为列 参数二为行
                        // 给保障内容赋值 参数一为当前保障内容项,参数二为序号,参数三是外出机组保障车的数据
                        context = new ParamsList().getContext(value, index.get(tag), todayoutbaozhang);
                        // 司机
                        String Beuzhu = context.get("Beuzhu");
                        if (Beuzhu != null) {
                            key = DailyExcel.getKey(lieBiao3, "司机"); // 从返回数组获取
                            column = key; // 8
                            writer.writeCellValue(column, x, Beuzhu);
                        } else {
                            key = DailyExcel.getKey(lieBiao3, "司机"); // 从返回数组获取
                            column = key; // 8
                            writer.writeCellValue(column, x, "");
                        }
                        // 里程数
                        String Jiaci = context.get("Jiaci");
                        if (Jiaci != null) {
                            key = DailyExcel.getKey(lieBiao3, "里程数"); // 从返回数组获取
                            column = key; // 6
                            writer.merge(row, row, column, column + 1, Jiaci, false);
                        } else {
                            key = DailyExcel.getKey(lieBiao3, "里程数"); // 从返回数组获取
                            column = key; // 6
                            writer.merge(row, row, column, column + 1, "", false);
                        }
                        tag++;
                    }
                }

                // 车号
                key = DailyExcel.getKey(lieBiao3, "车号");
                column = key; // 4
                writer.writeCellValue(column, row, carNum.get(temp)); // 从返回数组获取
                // 机组号
                key = DailyExcel.getKey(lieBiao3, "机组号");
                column = key; // 3
                writer.writeCellValue(column, row, jizuhao); // 从返回数组获取
                // 序号
                key = DailyExcel.getKey(lieBiao3, "序号");
                column = key; // 2
                writer.writeCellValue(column, row, index.get(temp)); // 从返回数组获取
                row += 1;
                temp++;
            }
            // 获取行驶总里程数（公里）
            Integer keys = DailyExcel.getKey(lieBiao3, "序号");
            Integer keye = DailyExcel.getKey(lieBiao3, "保障内容");
            writer.merge(row, row, keys, keye, "行驶总里程数（公里）", false);
            // 司机
            context = new ParamsList().getzongji(Lists.BaoZhangNeiRong2().get(0), todayoutbaozhang);

            if (ObjectUtil.isNotEmpty(context.get("Beuzhu"))) {
                key = DailyExcel.getKey(lieBiao3, "司机"); // 从返回数组获取
                column = key; // 8
                writer.writeCellValue(column, row, context.get("Beuzhu"));
            } else {
                key = DailyExcel.getKey(lieBiao3, "司机"); // 从返回数组获取
                column = key; // 8
                writer.writeCellValue(column, row, "");
            }
            // 里程数
            String Jiaci = context.get("Jiaci");
            if (ObjectUtil.isNotEmpty(Jiaci)) {
                key = DailyExcel.getKey(lieBiao3, "里程数"); // 从返回数组获取
                column = key; // 6
                writer.merge(row, row, column, column + 1, Jiaci, false);
            } else {
                key = DailyExcel.getKey(lieBiao3, "里程数"); // 从返回数组获取
                column = key; // 6
                writer.merge(row, row, column, column + 1, "", false);
            }

            // 车型
            row = flag; // 56
            key = DailyExcel.getKey(lieBiao3, "车型");
            column = key; // 1
            writer.merge(row, row + jizuNum.size(), column, column, "保障车", false); // 从返回数组获取
            // 合并表头(纵)
            key = DailyExcel.getKey(lieBiao, "事项");
            column = key; // 0
            writer.merge(row, row + jizuNum.size(), column, column, "外出机组车辆保障", false); // 从返回数组获取
            // ====================================================================================
            // 以下文件头设置,都为.xls
            response.setContentType("application/vnd.ms-excel");
            // sitevehicleinformation.xls是弹出下载对话框的文件名，不能为中文，中文请自行编码
            response.setHeader("Content-Disposition", "attachment;filename=excel.xls");
            out = response.getOutputStream();
            writer.flush(out, true);
            return ResultGenerator.genOkResult("导出成功");
        } catch (RuntimeException | IOException e) {
            log.error("导出异常===========>" + e.getLocalizedMessage());
            return ResultGenerator.genFailedResult("导出失败");
        } finally {
            // 关闭writer，释放内存
            writer.close();
            // 关闭输出Servlet流
            IoUtil.close(out);
        }

    }

    // 根据所传列表集合和列表字段获取列号
    public static Integer getKey(Map<Integer, String> map, String value) {
        Integer Key = null;
        Set<Integer> integers = map.keySet();
        for (Integer integer : integers) {
            if (map.get(integer).equals(value)) {
                Key = integer;
            }
        }
        return Key;
    }

    /**
     * 把时间戳转化为年月日格式
     *
     * @param stime
     * @return
     */
    private Date getSDate(String stime) {
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
        Long s = Long.valueOf(stime);
        Date date = new Date(s);
        String format = sf.format(date);
        try {
            return sf.parse(format);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 获取传入日期的明天
     *
     * @param date
     * @return
     */
    private Date getMingtian(Date date) {
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        calendar.add(Calendar.DATE, 1);
        Date time = calendar.getTime();
        return time;
    }
}