# maven

### 第一步,创建一个maven项目(可用maven的quickstart骨架,当然干净的也可以)

![image-20200617163511888](maven和gradle的打包.assets\image-20200617163511888.png)

### 第二步,打开pom文件

在pom文件中添加以下代码

```xml
<parent>
	<groupId>org.springframework.boot</groupId>
	<artifactId>spring-boot-starter-parent</artifactId>
    <version>2.2.7.RELEASE</version>
</parent>
```

```xml
<build>
   <plugins>
       <plugin>
           <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-maven-plugin</artifactId>
       </plugin>
  </plugins>
</build>
```

### 最后刷新maven,点击maven中的package插件,就可以打出一个可执行的jar包了

==干净的maven工程需要自己创建一个可执行的类哦==



打包命令 

```linux
mvn package 

// 跳过测试打包
mvn package -Dmaven.test.skip=true
```

​	

---

# Gradle

### 第一步,创建一个gradle项目

### 第二步,打开build,gradle文件

### 第三步,在文件中的plugins中添加以下代码

![image-20200617164914946](maven和gradle的打包.assets\image-20200617164914946.png)

```java
id 'org.springframework.boot' version '2.2.7.RELEASE'
```

### 最后刷新gradle项目,点击gradle中的assemble插件就可以打出一个可执行的jar包了

![image-20200617165111481](maven和gradle的打包.assets\image-20200617165111481.png)

打包命令

```LINUX
gradle build

// 清除目录target文件并且跳过测试
gradle clean build -x test
```









# 测试运行

将maven或者gradle打出来jar包,放在桌面上,打开命令提示窗口,将路径设置在桌面上然后

```xml
java -jar <jar包名>
```

